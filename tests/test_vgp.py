import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['grid.color'] = '#666666'

import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
tfk = tfp.math.psd_kernels
print(tf.__version__)
if tf.__version__[0] == '2':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

import tensorflow_probability as tfp
tfd = tfp.distributions
tfk = tf.keras

import models.vgp as mvgp
import utils.plots as pl
import utils.data_generation as ut
import models.preprocessing as pp
dtype = np.float64


def test_VGP_training_sin_2d(save_model=True):
    CONFIG = os.path.join(os.path.dirname(__file__), '../artifacts/config', 'vgp-sin-2d.yaml')
    sin_data = pp.VGPCaseStudySinusoidData(CONFIG)
    sin_data.generate_training_data_sin_2d()

    vgp = mvgp.VGP.VGPBuilder(sin_data.get_VGP_parameters(), CONFIG)
    vgp.construct_tensorflow_graph()
    vgp.loss_function()
    training_metrics = vgp.train_model(save_model=save_model)
    (samples_, mean_, inducing_index_points_, variational_loc_) = training_metrics[-1]  # last_training_iteration     # s TODO 1.1
    pl.plot_VGP_training_sin_2d(
        ut.SinusoidData.sinusoid_1d_function,
        sin_data.obs_index_points[..., 0],  # (1, 300, 1) -> (1, 300)
        sin_data.obs,  # (1, 300)
        sin_data.inducing_index_points,  # (500, 1)
        variational_loc_,  # (1, 500)
        samples_[:,0,:],  # (20, 1, 500) -> (20, 500)
        mean_,  # (1, 500)
        sin_data.predictive_index_points,  # (500, 1)
        vgp.model_params.num_samples,  # 20
    )
    print('')


def test_VGP_training_sin_3d(save_model=False):
    # 1/ config used to create sin_data object
    # 2/ data generated or loaded -> needs to be direct call
    # 3/ VGP model created and instantiated with sin_data class
    # 4/ train, evaluate, loss
    CONFIG = os.path.join(os.path.dirname(__file__), '../artifacts/config', 'vgp-sin-3d.yaml')
    sin_data = pp.VGPCaseStudySinusoidData(CONFIG)
    sin_data.generate_training_data()
    vgp = mvgp.VGP.VGPBuilder(sin_data.get_VGP_parameters(), CONFIG)
    vgp.construct_tensorflow_graph()
    vgp.loss_function()
    vgp.train_model(save_model=save_model)
    vgp_error, vgp_accuracy, vgp_precision = vgp.get_error_precision_accuracy(
        sin_data.test_index_points, sin_data.test_obs
    )
    pl.plot_error_histogram(8, 4.5, vgp_error, f'VGP error distribution\nacc: {vgp_accuracy:.5f}, prec: {vgp_precision:.2f}')
    pl.plot_samples3D(12, 12, 0, vgp.predictive_index_points, vgp.samples_, vgp.obs_index_pts, vgp.obs)  # no samples yet
    pl.plot_sin3d_rand_points(
        ut.SinusoidData().sinusoid,
        sin_data.coordinate_range,
        sin_data.all_index_points,
        sin_data.all_observations,
        sin_data.num_training_points_
    )
    plt.show()
    print('')


def test_VGP_training_office_6d(save_model=False):
    """
        ----------------------------------------------------
        ## cvs pandas dataframe import ##
        ----------------------------------------------------
        ## 3D input GP fitting problem. ## (sinusoid is 3D)
        1. COORDINATES : project Z coord to XY plane 3D -> 2D
        2. TRACER : 1D
        3. obs : combined 1. & 2. -> 3D
        Coordinates and corresponding tracer value for 1 time step.
        4.1 scale - take average tracer over timesteps, remains 3D
        ----------------------------------------------------
        ## 6D input GP fitting problem ##
        4.2 scale - then scale GP dimensions to get
             Z coordinate,
             T time,
             P pressure ...
        ----------------------------------------------------
        ## Bijectors in tf ##
        transforming rand variables, between input and output rv.s
        unless data is generated by gaussian - assumption - can combine distributions
        forward - compute samples
        inverse - compute probabilities
        ----------------------------------------------------
        Challenges:
        - sampling with MCMC
        - switch to tf2.0
        - plot kernel and find better kernel from data
        ----------------------------------------------------
         ## Dimensions and replacements ##
        - t_idx : 1 D tracer
        - t_sel_idx : selection of close to section tracer training points
        - idx : training input
        - obs : output!
        - linsp : linspace
        - w : distance from BEGIN along section line
        - w_proj : altered xy to w when taking section cut
        - pred_idx : prediction input
        - pred_obs : prediction output
        - o : filler column full of 0s

        > FROM DATASET :
        obs_idx_pts (60, 2) : xy_idx
        obs (60, 1) : t_idx
        obs_rowvec  (60,) : t_idx.T
        pts (60, 3) : xyt_idx

        > CALCULATION :
        obs_proj_idx_pts (200, 1) : w_linsp
        sel_pts (7, 3) : xyt_sel_idx
        sel_obs (7,) : t_sel_idx
        ext_sel_pts (7, 4) : xytw_sel_idx
        train_idx_pts_along_section_line (7, 1) : w_sel_idx
        line_idx (200, 1) : w_pred_idx_linsp
        line_XY (200, 3) : xyo_pred_idx_linsp
        line_obs (200,) : t_pred_obs_line_linsp
        pred_idx_pts (2500, 2) xy_pred_pts
        pred_sel_idx_pts (7, 1) t_pred_pts
    """
    CONFIG = os.path.join(os.path.dirname(__file__), '../artifacts/config', 'vgp-office-6d.yaml')
    office_data = pp.VGPCaseStudyOfficeData(CONFIG)
    office_data.load_training_data(os.path.join(os.path.dirname(os.path.dirname(__file__)), office_data.path_to_data))

    vgp = mvgp.VGP.VGPBuilder(office_data.get_VGP_parameters(), CONFIG)
    vgp.construct_tensorflow_graph()
    vgp.loss_function()
    vgp.train_model(save_model=save_model)

    # doesnt need to generalize if we have observations i
    # VGP reproduction error - average = 0 and standard deviation bit larger than if we fitted with GP
    vgp.set_normalization_params(office_data.columns_mean, office_data.columns_stdev, office_data.tracer_mean, office_data.tracer_stdev)
    norm_test_index_points = vgp.normalize_data(office_data.test_obs_index_points)
    # norm_test_obs = vgp.normalize_data(office_data.test_obs)
    vgp_error, vgp_accuracy, vgp_precision = vgp.get_error_precision_accuracy(
        norm_test_index_points, office_data.test_obs
    )

    pl.plot_error_histogram(8, 4.5, vgp_error, f'VGP error distribution\nacc: {vgp_accuracy:.5f}, prec: {vgp_precision:.2f}')
    pl.plot_samples3D(12, 12, 0, vgp.predictive_index_points, vgp.samples_, vgp.obs_index_pts, vgp.obs)
    plt.show()
    print('')


if __name__ == "__main__":
    # test_VGP_training_sin_2d(save_model=True)
    test_VGP_training_sin_3d(save_model=True)
    # test_VGP_training_office_6d(save_model=False)


import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D  # REQ: projection='3d'
import numpy as np
import pandas as pd
import os
import pytest

import utils.plots as pl

class TestPlotGpVgpComparisons:
    @pytest.fixture
    def gp_vgp_data(self):
        # Data generated along noisy sinusoid
        # time, loss, precision, accuracy
        data = os.path.join(os.path.dirname(__file__), '../artifacts/data', 'gp-vgp-4000pts-loss-precision-accuracy.bak.csv')
        df = pd.read_csv(data, encoding='utf-8', engine='c')
        return df

    def test_plot_gp_vgp_comparison_train_time_against_inputs(self, gp_vgp_data):
        plt.figure(figsize=(6, 4))
        # ~ number of training pts = (N-30)/20
        X = [t * 20 + 30 for t in range(1, 1+len(gp_vgp_data.gp_precision))]
        plt.plot(X, gp_vgp_data.GP_time, label='GP_time')
        plt.plot(X, gp_vgp_data.VGP_time, label='VGP_time')
        leg = plt.legend(loc='upper right')
        plt.grid(True)
        for lh in leg.legendHandles:
            lh.set_alpha(0.5)
        plt.xlabel("No. training pts")
        plt.ylabel("time [sec]")
        plt.title("Gaussian Process and Variational Gaussian Process\n training times against number of training inputs")
        plt.show()
        # fig_t = plt.figure(figsize=(6, 4))
        # fig_t.savefig('vector_plots/time.eps', format='eps')

    def test_plot_gp_vgp_comparison_log_prob_loss(self, gp_vgp_data):
        plt.figure(figsize=(6, 4))
        X = [t * 20 + 30 for t in range(1, 1+len(gp_vgp_data.gp_precision))]
        plt.plot(X, gp_vgp_data.GP_loss, label='GP_loss')
        plt.plot(X, gp_vgp_data.VGP_loss, label='VGP_loss')
        plt.grid(True)
        leg = plt.legend(loc='upper right')
        for lh in leg.legendHandles:
            lh.set_alpha(0.5)
        plt.xlabel("No. training pts")
        plt.ylabel("log_prob loss")
        plt.title("GP and VGP log_prob loss")
        plt.show()

    def test_plot_gp_vgp_comparison_accuracy(self, gp_vgp_data):
        X = [t * 20 + 30 for t in range(1, 1+len(gp_vgp_data.gp_precision))]
        plt.plot(X, gp_vgp_data.gp_accuracy, label='GP_accuracy')
        plt.plot(X, gp_vgp_data.vgp_accuracy, label='VGP_accuracy')
        plt.grid(True)
        leg = plt.legend(loc='upper right')
        for lh in leg.legendHandles:
            lh.set_alpha(0.5)
        plt.xlabel("No. training pts")
        plt.ylabel("Accuracy")
        plt.title("Gaussian Process and Variational Gaussian Process\n test accuracies against number of training inputs")
        plt.show()

    def test_plot_gp_vgp_comparison_precision(self, gp_vgp_data):
        plt.figure(figsize=(6, 4))
        X = [t * 20 + 30 for t in range(1, 1+len(gp_vgp_data.gp_precision))]
        plt.plot(X, gp_vgp_data.gp_precision, label='GP_precision')
        plt.plot(X, gp_vgp_data.vgp_precision, label='VGP_precision')
        leg = plt.legend(loc='upper right')
        plt.grid(True)
        for lh in leg.legendHandles:
            lh.set_alpha(0.5)
        plt.xlabel("No. training pts")
        plt.ylabel("Precision")
        plt.title("Gaussian Process and Variational Gaussian Process\n"
                  " test precision against number of training inputs")
        plt.show()


def test_plot_correlated_and_independent_rand_vars():
    x = np.random.normal(size=500000)
    y = x * 0 + np.random.normal(size=500000)
    z = -x * 1 + np.random.normal(size=500000)

    fig3, ax3 = plt.subplots(1,2, figsize=(10,5))
    ax3[0].hist2d(x, y, bins=(40, 40), cmap=plt.cm.PuBu)
    ax3[1].hist2d(x, z, bins=(40, 40), cmap=plt.cm.PuBu)

    ax3[0].set_xlabel('Y')
    ax3[0].set_ylabel('X')
    ax3[0].set_title('Independent X, Y')
    ax3[1].set_xlabel('Z')
    ax3[1].set_title('Correlated X, Z')
    plt.show()


def add_subplot(sub_fig, title_str,
                index_points_v, samples_v, mean_v, true_fun,
                inducing_index_points_v, variational_loc_v,
                x_train_v, y_train_v):

    if isinstance(sub_fig, plt.Figure):
        print('use fig.add_axes() as sub_fig here')
        assert False
    else:
        sub_fig.set_title(title_str)

    sub_fig.scatter(inducing_index_points_v[..., 0], variational_loc_v,
                    marker='x', s=50, color='k', zorder=10,
                    label='variational_loc_')
    sub_fig.scatter(x_train_v[..., 0], y_train_v,
                    color='#00ff00', alpha=.1, zorder=9,
                    label='y_train_')

    num_samples_v = samples_v.shape[0]
    idx_pts_v = np.tile(index_points_v[..., 0], [num_samples_v, 1])
    sub_fig.plot(idx_pts_v.T, samples_v.T,
                 color='r', alpha=.05,
                 label=None)

    sub_fig.plot(index_points_v, mean_v, color='k',
                 label='mean_')
    sub_fig.plot(index_points_v, true_fun(index_points_v), color='b',
                 label='f(index_points_)')

    leg_ = sub_fig.legend(loc='upper right')
    for lh_ in leg_.legendHandles:
        lh_.set_alpha(1)


def add_contour(ax, fig, title_str,
                index_points_v,
                samples_v, mean_v,
                inducing_index_points_v, variational_loc_v,
                x_train_v, y_train_v):
    if isinstance(ax, plt.Figure):
        print('use fig.add_axes() as sub_fig here')
        assert False
    else:
        ax.set_title(title_str)
    axisX, axisY, axisZ = pl.idx_to_grid(index_points_v, mean_v)
    # ax.contour(axisX, axisY, axisZ, cmap='Blues', label='mean')
    ax.pcolor(axisX, axisY, axisZ, cmap='Greys', label='mean')
    leg_ = ax.legend(loc='upper right')
    for lh_ in leg_.legendHandles:
        lh_.set_alpha(1)


def add_3d(ax, fig, title_str,
           index_points_v, samples_v, mean_v, true_fun,
           inducing_index_points_v, variational_loc_v,
           x_train_v, y_train_v, edges=100):
    if isinstance(ax, plt.Figure):
        print('use fig.add_axes() as sub_fig here')
        assert False
    else:
        ax.set_title(title_str)
    axisX, axisY, axisZ = pl.idx_to_grid(index_points_v, mean_v,
                                      xedges=edges,
                                      yedges=edges)
    ax.plot_surface(axisX, axisY, axisZ, cmap=cm.coolwarm,
                    linewidth=0, antialiased=False, label='mean')


def make_axis_3d(fig_rows=1, fig_cols=1, **kwargs):
    figsize = kwargs.pop('figsize', (15, 15))
    figkws = {'figsize': figsize}
    fig = plt.figure(**figkws)
    kwargs['projection'] = '3d'
    ax = np.empty_like(prototype=np.ones([fig_rows, fig_cols]),
                       dtype=Axes3D)
    for i in range(fig_rows*fig_cols):
        ax_ij = fig.add_subplot(fig_rows, fig_cols, i+1, **kwargs)
        ax[i//fig_cols, i%fig_cols] = ax_ij
    return fig, ax


if __name__ == "__main__":
    pytest.main([__file__, '-k', 'test_plot_gp_vgp_comparison_log_prob_loss', '-vv'])
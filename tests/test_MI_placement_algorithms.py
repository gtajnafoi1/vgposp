import time
import os
import numpy as np
import pandas as pd
import tensorflow as tf

import models.preprocessing
from models.MI_placement_algorithms import if_denom_is_near_zero, if_delta_cached_is_not_uptodate, then_update_delta_y, \
    append_to_A_remove_from_A_bar, if_delta_cached_is_uptodate

import utils.plots as pl
import models.MI_placement_algorithms as mi
import models.sensor_placement as sp
import models.preprocessing as pp


def load_cov_vv(file_name='cov_vv.csv'):
    df = pd.read_csv(file_name, encoding='utf-8', engine='c')
    cov_vv = np.array(df.iloc[:, 1:])
    # times = list(df['times'])
    return cov_vv


def save_cov_vv(cov_vv, file_name='cov_vv.csv'):
    df = pd.DataFrame(cov_vv)
    df.to_csv(file_name)


def test_save_cov_vv():
    dim = 11
    U = np.random.normal(1, 1, size=(dim, dim))
    U_UT = np.dot(U, U.T)
    cov_vv_np = U_UT  + 0.001*np.eye(dim)  # Sigma_pos_definite + diagonal(1)
    save_cov_vv(cov_vv_np, 'test_cov_vv.csv')
    cov_vv_back = load_cov_vv('test_cov_vv.csv')
    assert np.allclose(cov_vv_back, cov_vv_np)


# TEST VIII. ========================================================
def test__if_denom_is_near_zero(nom, denom):

    denom_is_near_zero = tf.function(if_denom_is_near_zero)(nom, denom)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print(' denom_is_near_zero: ', sess.run(denom_is_near_zero), nom, denom)


# TEST VII. ========================================================
def test__if_delta_cached_is_not_uptodate(cov_vv, updated, go):

    INF = tf.constant(1e10, dtype=tf.float64)
    N = cov_vv.shape[0]
    A = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))
    A_bar = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))

    values01 = tf.cast(tf.linspace(0., tf.cast(cov_vv.shape[0]-1, tf.float32), cov_vv.shape[0]), tf.int64) # values
    values02 = tf.cast(tf.linspace(0., 0., cov_vv.shape[0]), tf.int64)  # values
    indxes = tf.stack([values01, values02], axis=1)
    V = tf.SparseTensor(indxes, values01, [cov_vv.shape[0], 1])

    v = 3
    y_st = tf.SparseTensor(indices=[[v, 0]], values=[tf.cast(v, dtype=tf.int64)], dense_shape=(N, 1))
    # A = tf.sets.union(A, y_st)
    A_bar = tf.sets.difference(V, A)

    delta_cached = tf.Variable(tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)),
                               dtype=tf.float64,
                               shape=[N, 1])
    delta_cached_uptodate = tf.Variable(tf.fill([N, 1], updated), shape=[N, 1])

    # cov_vv = snippets.tf_print2(cov_vv, [y_st.values, A.values, A_bar.values], 'test: A, A_bar=\n')

    # if_delta_cached_is_not_uptodate_ = tf.function(if_delta_cached_is_not_uptodate)

    # delta_cached2, delta_cached_uptodate2, cov_vv2 = if_delta_cached_is_not_uptodate(
    delta_cached2, delta_cached_uptodate2 = if_delta_cached_is_not_uptodate(
        y_st, A, A_bar, cov_vv,
        delta_cached,
        delta_cached_uptodate, go=tf.constant(go))

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        writer = tf.summary.FileWriter('test__if_delta_cached_is_not_uptodate')
        writer.add_graph(sess.graph)

        # TODO: here cov_vv2 is forcing if_delta_cached_is_not_uptodate to be called at all
        # maybe other than the Variables inside will be needed
        # print('idcnu_1: \n', sess.run([cov_vv2, delta_cached2, delta_cached_uptodate2]))
        print('idcnu_-: \n', sess.run([delta_cached, delta_cached_uptodate]))
        print('idcnu_1: \n', sess.run([delta_cached2, delta_cached_uptodate2]))
        print('^^^ updated=', updated, 'go=', go, 'expected to update cache[3] if not updated and go\n')


# TEST IX. ========================================================
def test__then_update_delta_y(dnz, go):

    # delta_y = 0
    denom_is_near_zero = tf.constant(dnz)
    true_ = tf.constant(go)
    nom = tf.constant(0.1)
    denom = tf.constant(0.001)

    delta_y_ = then_update_delta_y(  # TODO tf.function
        denom_is_near_zero,  # True : don't do anything
        true_,               # False : don't do anything
        nom,
        denom)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print(' - ')
        print(' denom_is_near_zero: ', sess.run(denom_is_near_zero))
        print(' true_: ', sess.run(true_))
        print(' delta_y_: ', sess.run(delta_y_))  # expect 100.


# TEST X. ========================================================
def test__append_to_A_remove_from_A_bar(cov_vv):

    N = cov_vv.shape[0]
    A = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))
    A_bar = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))
    # y_st = tf.SparseTensor(indices=[[1, 0]], values=[tf.cast(1, dtype=tf.int64)], dense_shape=(N, 1))
    i = tf.cast(tf.constant(0), dtype=tf.int64)
    y_st = tf.SparseTensor(indices=[[i, 0]], values=[i], dense_shape=[N, 1])
    A_bar = tf.sets.union(A_bar, y_st)
    A_, A_bar_ = append_to_A_remove_from_A_bar(A, A_bar, y_st)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print(' A_: ', sess.run(A_.values))
        print(' A_bar_: ', sess.run(A_bar_.values))


# TEST VI. ========================================================
def test__if_delta_cached_is_uptodate(cov_vv, cache_vals=True):

    INF = tf.constant(1e36)
    N = cov_vv.shape[0]
    delta_y = 0.1

    i = tf.cast(0, dtype=tf.int64)
    y_st = tf.SparseTensor(indices=[[i, 0]], values=[i], dense_shape=[N, 1])

    delta_cached_uptodate = tf.Variable(tf.fill([N, 1], cache_vals), shape=[N, 1])
    op2 = tf.scatter_nd_update(delta_cached_uptodate, [[y_st.values[0], 0]], [cache_vals])  # deltdelta_cached_uptodate[y_st] = True

    with tf.control_dependencies([op2]):
        bool_uptodate = if_delta_cached_is_uptodate(delta_cached_uptodate, y_st)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print('idcnu_0: ', sess.run(bool_uptodate))


def cov_vv_4x4():
    cov_vv = np.array(
        [[1.10, 0.31, 0.33, 0.27],
         [0.31, 1.01, 0.30, 0.27],
         [0.33, 0.30, 0.97, 0.33],
         [0.27, 0.27, 0.33, 1.2]])
    return cov_vv


def calculate_running_time_algorithm_2(cov_vv, k):
    strt = time.time()
    # pl = tfplacement_algorithm_2(cov_vv, k)
    pl = mi.placement_algorithm_2(cov_vv, k)
    print("placement_array : ",pl)
    end = time.time()
    return (end - strt)


def test_algorithm_2():
    """algorithm 1 improved with priority queue
    gpf.placement_algorithm_2

    we time the algorithm for each increase in the size of the cov input
    """
    k = 5
    num_of_cov_sizes = 40
    n_sizes = np.linspace(10, num_of_cov_sizes, num_of_cov_sizes - 10, dtype=int)

    # n_array = np.array([10,20,30,40,50])
    n_array = np.array([])
    for i in n_sizes:
        n_array = np.append(n_array, i)

    times_array = np.array([])
    for n in n_array:
        n = int(n)
        cov = mi.dg_create_random_cov(n)  # ndarray
        r_time = calculate_running_time_algorithm_2(cov, k)  # Algorithm 2
        times_array = np.append(times_array, r_time)

    pl.plts_plot_algorithm_scale(12, 4, n_array, times_array)


def test_placement_algorithm_1():
    cov_vv = cov_vv_4x4()
    A = mi.placement_algorithm_1(cov_vv, 4)
    print('algo1 A: ', A)


def test_placement_algorithm_2():
    cov_vv = cov_vv_4x4()
    A = mi.placement_algorithm_2(cov_vv, 4)
    print('algo2 A: ', A)


global COVER_spatial
global GEN_LOCAL_kernel
global BETA_val

indirection = {
    'COVER_spatial': [25, 25, 5],
    'GEN_LOCAL_kernel': True,
    'BETA_val': 4.,
    'cutoff': 3  # int32
}


def run_sparse_algo_2(office_data, flag_use_4x4=False):
    COVER_spatial, file_cov_vv, file_cov_idxs, file_delt_ch_it, _, \
    GEN_LOCAL_kernel, BETA_val = office_data.get_spatial_splits()

    if flag_use_4x4:
        cov_vv_np = cov_vv_4x4()
    else:
        # COVER_spatial = [10, 10, 1]
        N = COVER_spatial[0]*COVER_spatial[1]*COVER_spatial[2]
        cov_vv_np = load_cov_vv(file_cov_vv)[:N, :N]

    cov_vv = tf.constant(cov_vv_np)
    A, len_A, delta_cached_iters = mi.sparse_placement_algorithm_2(cov_vv, 8, COVER_spatial)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print(' ')
        [A_values_, A_len_, delta_cached_iters_] = sess.run([A.values, len_A, delta_cached_iters])
        print('placement: ', A_values_, A_len_)
        # print('delta_cached_iters: \n', delta_cached_iters_)
        pd.DataFrame(delta_cached_iters_).to_csv(file_delt_ch_it)

        if False:
            print('\n\nnp:')
            A_np, _, _, _ = mi.placement_algorithm_2(cov_vv_np, 8)
            print('placement np: ', A_np, len(A_np))
            # assert np.allclose(A_np, A_values_)


def test_algo_2_split_10x10x10_50x50x10(office_data):
    xyzdens = [[2, 2, 2],
               [3, 3, 3],
               [4, 4, 4],
               [5, 5, 5],
               [6, 6, 6],
               [7, 7, 7],
               [8, 8, 8],
               [9, 9, 9]]
    # [7, 7, 10],
    # [10, 10, 10],
    # [12, 12, 10],
    # [14, 14, 10],
    # [16, 16, 10],
    # [18, 18, 10],
    # [20, 20, 10],
    # [22, 22, 10],
    # [24, 24, 10],
    # [26, 26, 10],
    # [28, 28, 10],
    # [30, 30, 10],
    # [35, 35, 10],
    # [40, 40, 10],
    # [45, 45, 10],
    # [50, 50, 10],
    # [55, 55, 10],
    # [60, 60, 10],
    # [65, 65, 10],
    # [70, 70, 10],
    # [75, 75, 10]]

    times_MAIN = np.array([]).astype(float)
    sensor_MAIN = np.array([]).astype(float)
    beta_MAIN = np.array([]).astype(float)

    first_time = True
    for cs in xyzdens:
        indirection['COVER_spatial'] = cs
        indirection['GEN_LOCAL_kernel'] = False
        indirection['BETA_val'] = None

        COVER_spatial_, file_cov_vv, file_cov_idxs, \
        file_delt_ch_it, SAVE_SELECTION_file, \
        GEN_LOCAL_kernel_, BETA_val_ \
            = office_data.get_spatial_splits()

        print(COVER_spatial_)
        tf.reset_default_graph()

        start_time_MAIN = time.time()
        sensor_selection = sp.RUN_cov_generation(indirection, grid=...)

        end_time_MAIN = time.time()
        diff_time_MAIN = int(end_time_MAIN - start_time_MAIN)

        times_MAIN = np.append(times_MAIN, diff_time_MAIN)
        sensor_MAIN = np.append(sensor_MAIN, sensor_selection)


        d = {'cover': [COVER_spatial_],
             'time': [diff_time_MAIN],
             'selection': [sensor_selection]}
        df = pd.DataFrame(data=d)

        file = './main_tests/train-data/alg2_fullkernel_results.csv'
        tempfile = './main_tests/train-data/alg2_fullkernel_results_temp.csv'
        if first_time:
            df.to_csv(file)
            first_time = False
        else:
            df_prev = pd.read_csv(file)
            df_prev.drop(columns=['Unnamed: 0'], inplace=True)
            df_prev = df_prev.append(df)
            df_prev = df_prev.reset_index(drop=True)
            df_prev.to_csv(file)

            #
            # with open(tempfile, 'rt') as f:
            #     head = f.read()
            #     row = f.read()
            #     # silent f.close()
            # with open(file, 'a+') as f:
            #     f.write(row)


def TEST_cov_2_equal_cov_3(office_data, optimal_grid_values):
    #----------------------------------------------------------
    # dataframe export paths
    #----------------------------------------------------------
    SAVE_CACHE_alg2test = 'main_datasets_tests/placement_algorithm2_cache_test.csv'
    SAVE_SELECTION_alg2test = 'main_datasets_tests/placement_algorithm2_selection_idxs_test.csv'
    SAVE_CACHE_alg3test = 'main_datasets_tests/placement_algorithm3_cache_test.csv'
    SAVE_SELECTION_alg3test = 'main_datasets_tests/placement_algorithm3_selection_idxs_test.csv'
    BETA_val = office_data.indirection['BETA_val']
    SET_ID = 3
    CSV_CELL = './dataset_2_cov/dataset_2_cov_' + str(SET_ID) + '.csv'
    SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z = 4, 4, 4  # SPLIT_cell_ property of file generation

    COVER_spatial_small = [5, 5, 1]
    # office_data.
    #----------------------------------------------------------
    # Generate cov 2, use algorithm 2
    #----------------------------------------------------------
    bool_cov2 = True
    selection_alg2_cov2, _, _ = sp.RUN_cov_generation(office_data, optimal_grid_values)

        # COVER_spatial_small, bool_cov2, CSV_CELL,
        #                                         SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z,
        #                                         SAVE_CACHE_alg2test, SAVE_SELECTION_alg2test,
        #                                         BETA_val, optimal_grid_values, office_data)

    #----------------------------------------------------------
    # Generate cov 3, use algorithm 2
    #----------------------------------------------------------
    bool_cov2 = False  # meaning create cov3
    selection_alg2_cov3, _, _ = sp.RUN_cov_generation(office_data, optimal_grid_values)

        # COVER_spatial_small, bool_cov2, CSV_CELL,
        #                                         SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z,
        #                                         SAVE_CACHE_alg3test, SAVE_SELECTION_alg3test,
        #                                         BETA_val, optimal_grid_values, office_data)  # -> cutoff = 3

    assert np.allclose(selection_alg2_cov2.values, selection_alg2_cov3.values)



if __name__ == '__main__':
    CONFIG = os.path.join(os.path.dirname(__file__), 'artifacts/config', 'vgp-office-6d.yaml')
    office_data = pp.VGPCaseStudyOfficeData(CONFIG)


    #test_placement_algorithm_1()
    #test_placement_algorithm_2()
    test_algorithm_2()
    #test_save_cov_vv()
    original_grid = models.preprocessing.init_grid(indirection)
    TEST_cov_2_equal_cov_3(office_data, original_grid)

    #test_algo_2_split_10x10x10_50x50x10(office_data)

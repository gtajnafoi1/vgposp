## VGPosp


### Virtual environment setup
```
conda create -n vpgosp37 python=3.7
conda activate vpgosp37 
pip install -r requirements.txt
```


### Tests
```
# set /home/path-to/vgpops/ as sources root to run tests
# set swap size > 0Mb:  sudo free -h
# sudo sysctl vm.swappiness=80

# Train VGP
/tests/test_vgp.py
# to configure a new example add config file:
#   /artifacts/config/vgp-office-6d.yaml

# Test Mutual Information Placement algorithm
/tests/test_MI_placement_algorithm.py

```

### Office case study
```

1. Train VGP
 - train using a testcase in test_vgp.py
 - paths/parameters set in config/vgp-office-6d.yaml
 - define where to save trained model: model_saver_path

2. Place sensors
 - sensor_placement.py : full_pass_with_wrapper 
 - generates spatial grid points to form superset of potential placement coordinates
 - loads pretrained VGP model to generate covariance matrix for placement algorithm
 - runs placement algorithm to find optimal coordinated for maximizing Mutual Information
 - tweaks original grid points to discover spatial regions with higher MI
 
A pretrained VGP model used for the case study in the thesis is
 available for use in /artifacts/checkpoints/vgp-office/
```
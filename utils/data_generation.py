import numpy as np
from typing import List


dtype = np.float64


class SinusoidData:

    # /////////////////////////////////////////// TODO
    @staticmethod
    def sinusoid(x: np.array) -> np.ndarray:
        """ R^N -> R this will be the 'true' function that
        will be approximated by the GP and the VGP
        Computes s1 = sin(x1) for each dimension

        input x, y grid
        output x, y, v
        """
        s = 0
        for i in range(x.shape[1]):
            s += np.sin(np.pi * x[:, i] / 1.5)  # eg [300,2]
        return s

    @staticmethod
    def generate_2d_sinusoid_training_data( # TODO remove
            num_training_points,
            observation_noise_variance,
            coord_range
    ) -> (np.ndarray, np.ndarray):

        """Generate noisy sinusoidal observations at a random set of points.
        Generate training data with a known noise level (we'll later try to recover
        this value from the data).

          y = f(x) + noise
        Returns:
           observation_index_points, observations
        """
        index_points = np.random.uniform(0., 1., (num_training_points, 2))
        assert(index_points.shape == (num_training_points, 2))
        index_points = index_points.astype(np.float64)

        x_scale = coord_range[0][1] - coord_range[0][0]
        index_points[:, 0] *= x_scale
        index_points[:, 0] += coord_range[0][0]
        y_scale = coord_range[1][1] - coord_range[1][0]
        index_points[:, 1] *= y_scale
        index_points[:, 1] += coord_range[1][0]
        print("index_points_[:, 0]", index_points[:, 0])
        print("index_points_[:, 1]", index_points[:, 1])

        noise = np.random.normal(loc=0,
                                 scale=np.sqrt(observation_noise_variance),
                                 size=(num_training_points))

        assert(noise.shape == (num_training_points,))
        observations = (SinusoidData().sinusoid(index_points) + noise)
        return index_points, observations

    @staticmethod
    def generate_3d_sin_index_observation_points(num_training_points, observation_noise_variance, coord_range) -> (np.ndarray, np.ndarray):
        """Generate noisy sinusoidal observations at a random set of points.
        Generate training data with a known noise level (we'll later try to recover
        this value from the data).
        Returns:
           observation_index_points, observations
        """
        index_points = np.random.uniform(0., 1., (num_training_points, 2))
        assert(index_points.shape == (num_training_points, 2))
        index_points = index_points.astype(np.float64)

        x_scale = coord_range[0][1] - coord_range[0][0]
        index_points[:, 0] *= x_scale
        index_points[:, 0] += coord_range[0][0]
        y_scale = coord_range[1][1] - coord_range[1][0]
        index_points[:, 1] *= y_scale
        index_points[:, 1] += coord_range[1][0]

        print("index_points_[:, 0]", index_points[:, 0])
        print("index_points_[:, 1]", index_points[:, 1])

        # y = f(x) + noise
        noise = np.random.normal(loc=0,
                                 scale=np.sqrt(observation_noise_variance),
                                 size=(num_training_points))

        assert(noise.shape==(num_training_points,))
        observations = (SinusoidData().sinusoid(index_points) + noise)
        return index_points, observations

    @staticmethod
    def generate_2d_index_points(
            num_training_points,
            coordinate_range=[[-2., 2.], [-2., 2.]],
    ) -> np.ndarray:
        """Generate noisy sinusoidal observations at a random set of points.
        Returns:
           obs_idx_pts: XY (num, 2)
        """
        [[xlow, xhigh], [ylow, yhigh]] = coordinate_range
        idx_pts = np.random.uniform(0., 1., (num_training_points, 2)).astype(dtype=dtype)  # XY
        assert(idx_pts.shape == (num_training_points, 2))
        idx_pts = idx_pts.astype(np.float64)
        x_scale = xhigh - xlow
        idx_pts[:, 0] *= x_scale
        idx_pts[:, 0] += xlow
        y_scale = yhigh - ylow
        idx_pts[:, 1] *= y_scale
        idx_pts[:, 1] += ylow
        return idx_pts[np.newaxis, ...]  # batch, num, coord  == returns pred_idx_pts

    @staticmethod
    def generate_predictive_index_points():
        pass

    @staticmethod
    def sinusoid_1d_function(x):  # TODO merge with other sinusoid method to make dim agnostic
        # from: lambda x: np.exp(-x[..., 0]**2 / 20.) * np.sin(1. * x[..., 0])
        return np.exp(-x[..., 0]**2 / 20.) * np.sin(1. * x[..., 0])

    @staticmethod
    def generate_1d_sin_data(
            num_training_points,
            observation_noise_variance,
            coordinate_range
    ):
        x_train_ = np.random.uniform(coordinate_range[0], coordinate_range[1], [num_training_points, 1])
        y_train_ = (
                SinusoidData.sinusoid_1d_function(x_train_) +
                np.random.normal(0., np.sqrt(observation_noise_variance), [num_training_points])
        )
        return x_train_, y_train_

    @staticmethod
    def generate_1d_inducing_index_points(num_predictive_index_points, coordinate_range):
        return np.linspace(coordinate_range[0], coordinate_range[1],
                           num_predictive_index_points,
                           dtype=dtype
                           )[..., np.newaxis]
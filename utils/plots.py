import sys
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from models.sensor_placement import PRINTS


def plot_sin3d_rand_points(sinusoid_fn, coord_range, obs_index_pts, obs, number_of_points):
    # Plot 3D sin
    fig = plt.figure(figsize=(10, 10))
    ax = fig.gca(projection='3d')
    stepx = (coord_range[0][1] - coord_range[0][0])/number_of_points # coord_range[0] = 'xrange', coord_range[1] = 'yrange'
    x = np.arange(coord_range[0][0], coord_range[0][1]+stepx, stepx)
    stepy = (coord_range[1][1] - coord_range[1][0])/number_of_points
    y = np.arange(coord_range[1][0], coord_range[1][1]+stepy, stepy)
    X, Y = np.meshgrid(x, y, sparse=False)
    # Z = np.sin((X)) * np.sin((Y))

    Z = np.zeros(X.shape)
    for j in range(Z.shape[1]):
        for i in range(Z.shape[0]):
            Z[i,j] = sinusoid_fn(np.array([[X[0,j],Y[i,0]]])) # draw above x,y, 1,2 shape

    ax.plot_surface(X, Y, Z, cmap=plt.cm.Reds,linewidth=0, antialiased=False, alpha=0.35)
    # ax.scatter(xyz_pts[:,0],xyz_pts[:,1],xyz_pts[:,2])
    ax.scatter(obs_index_pts[:,0], obs_index_pts[:, 1], obs)
    ax.set_xlabel('X', fontsize=13)
    ax.set_ylabel('Y', fontsize=13)
    ax.set_zlabel('np.sin(np.pi * coord[:, i] /1.5)', fontsize=13)
    ax.set_title('The training points and the true function', fontsize=18, horizontalalignment='center')


def idx_to_grid(idx_pts, fn_vals, xedges=50, yedges=50):
        idx_pts_x, idx_pts_y = idx_pts[..., 0],  idx_pts[..., 1]
        minx, maxx, miny, maxy = np.min(idx_pts_x), np.max(idx_pts_x), np.min(idx_pts_y), np.max(idx_pts_y)
        x_, y_ = np.linspace(minx, maxx, 1+xedges), np.linspace(miny, maxy, 1+yedges)
        axisX, axisY = np.meshgrid(x_, y_, sparse=False)  # like 20x20

        assert 3 == len(idx_pts.shape)
        assert 2 == len(fn_vals.shape)
        from scipy.interpolate import griddata
        axisZ = griddata(idx_pts[0, ...], fn_vals[0, ...], (axisX, axisY), method='nearest')

        # if we use other than nearest, we have to handle None-s!
        # method='nearest' : see
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.griddata.html

        return axisX, axisY, axisZ


def plot_samples3D(figx, figy, axis_j, pred_index, pred_samples, obs_index_pts, obs):
    # Generate a plot with
    #   - the posterior predictive mean
    #   - training data
    #   - inducing index points (plotted vertically at the mean of the variational
    #     posterior over inducing point function values)
    #   - 50 posterior predictive samples

    PRED_FRACTION = 300
    fig_ = plt.figure(figsize=(figx, figy))
    ax = fig_.gca(projection='3d')

    axisX, axisY, axisZ = idx_to_grid(pred_index,  # [np.newaxis, ...],
                                      pred_samples[0, ...],
                                      xedges=PRED_FRACTION,
                                      yedges=PRED_FRACTION)

    # the fitted surface (approximated on the grid as nearest, so there is some tiling-effect)
    ax.plot_surface(axisX, axisY, axisZ, cmap=plt.cm.Reds, linewidth=0, antialiased=False, alpha=0.4)

    # the training points
    ax.scatter(obs_index_pts[0, :, 0],
               obs_index_pts[0, :, 1],
               obs[0, :], color='g')

    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel('Approximation of the true fn', fontsize=13)
    ax.set_zlim(-2, 2)
    ax.set_title('The training points and the approximated fn', fontsize=18, horizontalalignment='center')
    # plt.show()


def plot_error_histogram(fw, fh, feature, title):
    plt.figure(figsize=(fw, fh))
    plt.hist(feature, bins=20)
    plt.title(title)
    plt.xlabel("error histogram")
    plt.ylabel("test data error")
    plt.xlim(-1, 1)
    # plt.show()


def calc_axisZ(edges):
    start, stop, step = 1/edges, 1, 1/edges
    edges_ = np.arange(start, stop+step, step)
    o_axis = np.linspace(start, stop, len(edges_))
    return o_axis


def plot_VGP_training_sin_2d(
        f,
        x_obs_index_points,
        y_obs,
        inducing_index_points,
        variational_loc,
        samples,
        mean,
        index_points,
        NUM_GP_SAMPLES,
        #coordinate_range
    ):
    # plts.plot_gp_linesamples(12, 4, index_points,  # (15,1)
    #                          f(index_points),  # (15,)
    #                          inducing_index_points,  # (100,1)
    #                          samples,  # (8,2,100)
    #                          NUM_GP_SAMPLES)  # (8)

    plt.figure(figsize=(15, 5))
    plt.scatter(inducing_index_points[..., 0], variational_loc, marker='x', s=50, color='k', zorder=10, label='variational_loc')
    plt.scatter(x_obs_index_points, y_obs, color='#00ff00', alpha=.1, zorder=9, label='y_train')
    plt.plot(np.tile(index_points, NUM_GP_SAMPLES), samples.T, color='r', alpha=.1, label=None)
    plt.plot(index_points, mean.T, color='k', label='mean')
    # plt.gca().fill_between(index_points, mean - s, mean + s, color="#dddddd")
    plt.plot(index_points, f(index_points), color='b', label='f(index_points)')

    leg = plt.legend(loc='upper right')
    for lh in leg.legendHandles:
        lh.set_alpha(1)
    plt.show()


def plts_plot_algorithm_scale(figx, figy, n_array, time_array):
    plt.figure(figsize=(figx, figy))
    length = time_array.shape[0]
    for i in range(length):
        plt.scatter(n_array[i], time_array[i], color='grey', s=40)  # observations
    plt.show()


def print_sel_and_coords(ok_, i_, sel_ordered_, grid_, kth_, MI_sum_k_, txt_file=None):
    coords = []
    for k in range(kth_):
        [i0_, i1_, i2_] = models.preprocessing.from_sel_to_grid_idxptr(sel_ordered_[k], grid_)
        coords.append([g for g in grid_[i0_, i1_, i2_, :]])
    ok_str = 'OK: i=' if ok_ else 'NO: i='
    print([ok_str, i_,
           'sel=', [s for s in sel_ordered_],
           'xyz=', coords,
           'MI_sum', MI_sum_k_,
           ], ',')
    if txt_file is not None:
        with open(txt_file, 'a') as f:
            s = ''.join([str([ok_str, i_,
                              'sel=', [s for s in sel_ordered_],
                              'xyz=', coords,
                              'MI_sum', MI_sum_k_]), ',\n'])
            f.write(s)


def tf_print2(op, tensors, message=None):
    def print_message2(*args):
        str_ = message
        if PRINTS:
            for s_ in [str(x) for x in args]:
                str_ += s_ + ', '
            sys.stdout.write(str_ + '\n')
        return args

    prints = tf.numpy_function(print_message2, [t for t in tensors], [t.dtype for t in tensors])
    with tf.control_dependencies(prints):
        op = tf.identity(op)
    return op
import pathlib
import time
import numpy as np
import tensorflow as tf
if tf.__version__[0] == '2':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

import tensorflow_probability as tfp
tfk = tfp.math.psd_kernels
tfd = tfp.distributions
import models.preprocessing as pp
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# use double precision throughout for better numerics.
dtype = np.float64


class TensorFlowGraph:
    def __init__(self):
        self.vgp_graph = None
        self.loss = None
        self.saver_VGP = None
        self.amplitude = None
        self.length_scale = None
        self.kernel = None
        self.inducing_index_points = None
        self.variational_loc = None
        self.variational_scale = None
        self.x_train_batch = None
        self.y_train_batch = None
        self.train_op = None
        self.log_likelihood = None
        self.obs_noise_variance = None


class VGP:
    @classmethod
    def VGPBuilder(cls, vgp_input_data, model_config_path):
        (inducing_index_points, obs_index_points, obs, predicted_index_pts, num_feature_dims) = vgp_input_data
        return cls(inducing_index_points, obs_index_points, obs, predicted_index_pts, num_feature_dims, model_config_path)

    def __init__(self,
                 inducing_index_points,
                 obs_index_pts: np.ndarray,
                 obs: np.ndarray,  # observations
                 predicted_index_pts,
                 num_feature_dims,
                 model_config_path
                 ):
        # input data
        self.inducing_index_points_ = inducing_index_points
        self.obs_index_pts = obs_index_pts
        self.obs = obs
        self.predictive_index_points = predicted_index_pts
        self.num_feature_dims = num_feature_dims

        # graph
        self.tf_graph = TensorFlowGraph()

        # model
        self.model_params = pp.ModelParameters(model_config_path)

        # trained model outputs:
        self.samples_ = None
        self.mean_ = None
        self.variational_loc_ = None

    def set_normalization_params(self, COLUMNS_MEAN, COLUMNS_STDEV, tracer_MEAN, tracer_STDEV):
        self.COLUMNS_MEAN = COLUMNS_MEAN
        self.COLUMNS_STDEV = COLUMNS_STDEV
        self.tracer_MEAN = tracer_MEAN
        self.tracer_STDEV = tracer_STDEV

    def normalize_data(self, train_dataset):
        central_train = train_dataset - self.COLUMNS_MEAN
        normal_train = central_train / self.COLUMNS_STDEV

        mean_check = np.average(normal_train, axis=0)
        stddev_check = np.std(normal_train, axis=0)
        assert np.allclose(mean_check, 0., atol=11) # 0.1
        assert np.allclose(stddev_check, 1., atol=1) # 0.1

        return normal_train

    def set_num_observations_to_num_prediction_indices(self):  # TODO code duplication
        # we need number of obs as pred_idx_pts to match
        K = self.predictive_index_points.shape[1]  # like 23*23=529
        k = self.obs.shape[1]  # == number of training points

        if k < K:
            obs_rep = self.obs[0, :]
            for _ in range(K//k):
                obs_rep = np.append(obs_rep, self.obs[0, :])
            i = 0
            while obs_rep.shape[0] < K:
                obs_rep = np.append(obs_rep, self.obs[0, i])
                i += 1
            obs_rep = obs_rep[:K]
            assert obs_rep.shape[0] == K
            self.obs = obs_rep

        else:
            obs_rep = self.obs[0, :K]
            assert obs_rep.shape[0] == K
            self.obs = obs_rep

    def construct_tensorflow_graph(self):
        # --------------------------------------------------
        # TF GRAPH
        # --------------------------------------------------
        # Create kernel with trainable parameters, and trainable observation noise
        # variance variable. Each of these is constrained to be positive.
        if self.tf_graph is None:
            raise Exception('Remember to set tensorflow model input variables.')
        self.tf_graph.amplitude = tf.Variable(.54, dtype=dtype, name='amplitude', use_resource=True)
        self.tf_graph.length_scale = tf.Variable(.54, dtype=dtype, name='length_scale', use_resource=True)

        self.tf_graph.amplitude_softplus = (tf.nn.softplus(self.tf_graph.amplitude))

        self.tf_graph.length_scale_softplus = (1e-5 + tf.nn.softplus(self.tf_graph.length_scale))

        self.tf_graph.kernel = tfk.MaternFiveHalves(
            amplitude=self.tf_graph.amplitude_softplus,
            length_scale=self.tf_graph.length_scale_softplus
        )
        self.tf_graph.obs_noise_variance = tf.Variable(
            .054,  # self.model_params.observation_noise_variance
            dtype=dtype,
            name='observation_noise_variance',
            use_resource=True)

        self.tf_graph.obs_noise_variance_softplus = tf.nn.softplus(self.tf_graph.obs_noise_variance)

        # Create trainable inducing point locations and variational parameters.
        self.tf_graph.inducing_index_points = tf.Variable(
            self.inducing_index_points_,
            dtype=dtype,
            name='inducing_index_points',
            use_resource=True
        )

        self.tf_graph.variational_loc, self.tf_graph.variational_scale = (  # variational_loc=mean, variational_scale=sigma
            tfd.VariationalGaussianProcess.optimal_variational_posterior(
                kernel=self.tf_graph.kernel,
                inducing_index_points=self.tf_graph.inducing_index_points,
                observation_index_points=self.obs_index_pts,
                observations=self.obs,  # this is using the whole train_data, not just a batch
                observation_noise_variance=self.tf_graph.obs_noise_variance_softplus
            )
        )

        # Constructs the VGP Distribution instance.
        self.tf_graph.vgp_graph = tfd.VariationalGaussianProcess(
            self.tf_graph.kernel,
            index_points=self.predictive_index_points,
            inducing_index_points=self.tf_graph.inducing_index_points,
            variational_inducing_observations_loc=self.tf_graph.variational_loc,
            variational_inducing_observations_scale=self.tf_graph.variational_scale,
            mean_fn=None,
            observation_noise_variance=self.tf_graph.obs_noise_variance_softplus,
            predictive_noise_variance=self.model_params.predictive_noise_variance,
            jitter=self.model_params.jitter,
            validate_args=False
        )

    def loss_function(self):
        # --------------------------------------------------
        # TF GRAPH
        # --------------------------------------------------
        # For training, we use numpy-based minibatching
        self.tf_graph.x_train_batch = tf.placeholder(dtype, [self.model_params.batch_size, self.num_feature_dims], name='x_train_batch')
        self.tf_graph.y_train_batch = tf.placeholder(dtype, [self.model_params.batch_size], name='y_train_batch')

        # Create the loss function we want to optimize.
        self.tf_graph.loss = self.tf_graph.vgp_graph.variational_loss(
            observations=self.tf_graph.y_train_batch,
            observation_index_points=self.tf_graph.x_train_batch,
            kl_weight=float(self.model_params.batch_size) / float(self.obs.shape[1]))  # self.obs.shape[1] == num_training_observations

        optimizer = tf.train.AdamOptimizer(learning_rate=self.model_params.learning_rate)
        self.tf_graph.train_op = optimizer.minimize(self.tf_graph.loss)

        # self.set_num_observations_to_num_prediction_indices()  -> TODO code duplication
        # we need as many as pred_idx_pts
        K = self.predictive_index_points.shape[1]  # like 23*23=529
        k = self.obs.shape[1]
        if k < K:
            obs_rep = self.obs[0, :]
            for _ in range(K//k):
                obs_rep = np.append(obs_rep, self.obs[0, :])
            i = 0
            while obs_rep.shape[0] < K:
                obs_rep = np.append(obs_rep, self.obs[0, i])
                i += 1
            obs_rep = obs_rep[:K]
        else:
            obs_rep = self.obs[0, :K]
        assert obs_rep.shape[0] == K

        self.tf_graph.log_likelihood = self.tf_graph.vgp_graph.log_prob(obs_rep[np.newaxis, ...])  # we need as many as pred_idx_pts
        for i in range(self.tf_graph.log_likelihood.shape[0]):
            tf.summary.scalar(f'self.log_likelihood[i] :', self.tf_graph.log_likelihood[i])


    def load_saved_variational_model(self, num_inducing_points_):
        print("LOGDIR", self.model_params.model_load_path)
        if self.model_params.model_load_path:
            self.tf_graph.variational_inducing_observations_loc_saved = tf.Variable(
                np.zeros([1, num_inducing_points_], dtype=dtype)
                + np.random.normal(0, 0.01, [num_inducing_points_]),
                name='variational_inducing_observations_loc_saved'
            )

            self.tf_graph.variational_inducing_observations_scale_saved = tf.Variable(
                np.reshape(np.eye(num_inducing_points_, dtype=dtype),
                           [1, num_inducing_points_, num_inducing_points_]),
                name='variational_inducing_observations_scale_saved'
            )

            saver_spec = {
                'variational_inducing_observations_loc_saved': self.tf_graph.variational_inducing_observations_loc_saved,
                'variational_inducing_observations_scale_saved': self.tf_graph.variational_inducing_observations_scale_saved,
                'amplitude': self.tf_graph.amplitude,
                'inducing_index_points': self.tf_graph.inducing_index_points,
                'length_scale': self.tf_graph.length_scale,
                'observation_noise_variance': self.tf_graph.obs_noise_variance
            }
            self.saver_VGP = tf.train.Saver(saver_spec)  # all the graph so far

    def train_model(
        self,
        num_iters: int=None,
        num_training_points_: int=None,
        num_samples: int=None,
        save_model=False
    ):  # -> List[ty.PlotItem]:
        # --------------------------------------------------
        # TF SESSION
        # --------------------------------------------------
        model_saver = os.path.join(os.path.dirname(os.path.dirname(__file__)), self.model_params.model_saver_path)
        if not num_iters:
            num_iters = self.model_params.num_iters
        if not num_training_points_:
            num_training_points_ = self.obs.shape[1]
        if not num_samples:
            num_samples = self.model_params.num_samples

        pathlib.Path(model_saver).mkdir(parents=True, exist_ok=True)
        summ = tf.summary.merge_all()  # TODO 1.1
        training_metrics = []
        saver = tf.train.Saver()

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            writer = tf.summary.FileWriter(model_saver)
            writer.add_graph(sess.graph)

            for i in range(num_iters):
                # shuffle 0..N-1 then take 0..batch_size
                training_idxs = np.linspace(0, num_training_points_-1, num_training_points_, dtype=int)
                np.random.shuffle(training_idxs)
                batch_idxs = training_idxs[:self.model_params.batch_size]

                # x_train, (100, 1)     x_train_batch, (64,1)  ->  (100,2)
                # y_train, (100,)       y_train_batch, (64,)   ->  (100,)
                x_train_batch_ = self.obs_index_pts[0, batch_idxs, ...]
                y_train_batch_ = self.obs[0, batch_idxs]

                [_, loss_] = sess.run([self.tf_graph.train_op, self.tf_graph.loss],
                                      feed_dict={self.tf_graph.x_train_batch: x_train_batch_,
                                                 self.tf_graph.y_train_batch: y_train_batch_})
                if i % (num_iters / self.model_params.num_logs) == 0 or i + 1 == num_iters:
                    print(i, loss_)

                    [
                        self.samples_,  # (20, 500)
                        self.mean_,  # (500,)
                        self.inducing_index_points_,  #  (10,1)
                        self.variational_loc_,  # (10,)
                        #s TODO 1.1
                    ] = sess.run([
                        self.tf_graph.vgp_graph.sample(num_samples),
                        self.tf_graph.vgp_graph.mean(),
                        self.tf_graph.inducing_index_points,
                        self.tf_graph.variational_loc,
                        #summ
                    ])
                    training_metrics.append(
                        (
                            self.samples_,
                            self.mean_,
                            self.inducing_index_points_,
                            self.variational_loc_,
                            #s TODO 1.1
                        ))
                    #writer.add_summary(s, i) TODO 1.1
                    writer.flush()

            if save_model:
                model_saved_path = saver.save(sess, model_saver)
                print('model_saved_path: ', model_saved_path)
        return training_metrics

    @staticmethod
    def get_nearest_for(trg_idx_vec, index_points, values):
        # find nearest x,y of training point to
        # [[x,y], [x,y], ...] : 1600
        # assert 2 == index_points.shape[1]
        N = index_points.shape[0]
        assert N == values.shape[0]

        if len(trg_idx_vec.shape) > 2:
            trg_idx_vec = trg_idx_vec.squeeze(0)
        trg_idx_vec = trg_idx_vec[:, :2]
        # assert 2 == trg_idx_vec.shape[1]
        M = trg_idx_vec.shape[0]
        nearest_pred = np.zeros(shape=(M), dtype=np.float64)

        # for the points in target
        for t in range(M):
            [tx, ty] = trg_idx_vec[t, :]
            # find the nearest point in index_points
            sdmin = 1000
            j = -1
            for i in range(N):
                [ix, iy] = index_points[i, :2]
                sq = np.square(tx - ix) + np.square(ty - iy)
                if sq < sdmin:
                    sdmin = sq
                    j = i
            assert -1 != j
            nearest_pred[t] = values[j]
        return nearest_pred

    def get_error_precision_accuracy(self, test_index_points, test_obs):
        vgp_test_nearest_pred = self.get_nearest_for(
            test_index_points, self.predictive_index_points[0, ...], self.samples_[0, 0, :]
        )
        vgp_error = test_obs - vgp_test_nearest_pred
        vgp_accuracy = np.average(vgp_error)  # the systematic error (not in %)
        vgp_precision = np.sqrt(np.average(np.square(vgp_error)))  # the scatter of the prediction error
        print('vgp_accuracy=', vgp_accuracy)
        print('vgp_precision=', vgp_precision)
        return vgp_error, vgp_accuracy, vgp_precision

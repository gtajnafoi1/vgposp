import datetime
import time
import sys
import os
import numpy as np
import pandas as pd
import tensorflow as tf

if tf.__version__[0] == '2':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

import tensorflow_probability as tfp
tfk = tf.keras
tfkb = tf.keras.backend
tfkl = tf.keras.layers
tfb = tfp.bijectors
tfd = tfp.distributions
tfpl = tfp.layers


import tests.test_MI_placement_algorithms as tmp
import models.MI_placement_algorithms as mi
import models.vgp as mvgp
import models.preprocessing as pp
import models.validation as va


dtype=np.float64
PRINTS = True


def change_optimal_grid(sel, orgn_grid, epsilon):
    # 1. Find grid-point prev_sel  (after an iteration, prev_sel is previously selected optimal points)
    # 2. modify gridpoint and return using grid system of normalized coordinates
    r0 = (np.random.random(1) - 0.5) * 2  # r = np.random.rand(([1, 3])) # - 0.5)* epsilon
    r1 = (np.random.random(1) - 0.5) * 2
    r2 = (np.random.random(1) - 0.5) * 2
    opt_grid_eps = np.copy(orgn_grid)  # np.zeros((COVER_spatial[0], COVER_spatial[1], COVER_spatial[2], 3))
    # v0, v1, v2 = pp.from_sel_idx_in_grid_to_normalized_coordinate(sel, orgn_grid)
    [i0, i1, i2] = pp.from_sel_to_grid_idxptr(sel, orgn_grid)

    shift_coordinate = np.array([r0, r1, r2]) * epsilon
    # assert opt_grid_eps[i0, i1, i2, 0] == v0
    a = opt_grid_eps[i0, i1, i2, 0] + shift_coordinate[0]
    b = opt_grid_eps[i0, i1, i2, 1] + shift_coordinate[1]
    c = opt_grid_eps[i0, i1, i2, 2] + shift_coordinate[2]
    opt_grid_eps[i0, i1, i2, 0] = a
    opt_grid_eps[i0, i1, i2, 1] = b
    opt_grid_eps[i0, i1, i2, 2] = c
    return opt_grid_eps


def optimize_grid_for_last_k(office_data, orig_grid, TRIAL_NUM, epsilon, vgp):
    # Hyperparameters of grid edits

    num_k_sensors = office_data.indirection.K_SENSORS

    # Create a BENCHMARK Mutual Information=delta for selection with original grid
    sel, dci, selection_and_delta = RUN_cov_generation(office_data, orig_grid, vgp)  # sel is only done in this function for k=k_num eg 1, 2 placements
    delta = np.amax(dci)
    delta_updates = delta
    delta_all_attempts = delta
    grid = orig_grid
    sel_and_delta = selection_and_delta
    sel_ordered = selection_and_delta[:, 0]
    MI_sum_k = np.sum(sel_and_delta[:, 1])  # compute sum of delta column up to the kth sensor that is optimized

    [i0, i1, i2] = pp.from_sel_to_grid_idxptr(sel_ordered[num_k_sensors - 1], grid)
    # print('i=', -1, 'sel=', sel, 'xyz=', grid[i0, i1, i2, :], 'MI_sum', MI_sum_temp)
    print(['OK: i=', -1,
           'sel=', [s for s in sel],
           'xyz=', [g for g in grid[i0, i1, i2, :]],
           'MI_sum', MI_sum_k], ',')

    # Try to improve MI=delta with edited grids
    # TRIAL_NUM number of attempts to improve MI and therefore improve grid
    stuck_counter = 0
    stuck_counter_limit = 3 * TRIAL_NUM  # the runtime may be affected to be 3+1 times as much ...
    i = 0
    while i < TRIAL_NUM:
        # Make a new grid, 1 pt change, change only the kth placement, given by k_num
        grid_temp_move1pt = change_optimal_grid(sel_ordered[num_k_sensors - 1], grid, epsilon)  # prev_sel, orgn_grid, epsilon

        # Test new grid for MI, selection_and_delta_move1pt contains all MI=delta results for each placement
        sel_temp_move1pt, dci_temp_move1pt, selection_and_delta_move1pt = RUN_cov_generation(office_data, grid_temp_move1pt, vgp)
        sel_ordered_move1pt = selection_and_delta_move1pt[:, 0]

        # Get MI max value from Test
        delta_temp_move1pt = np.amax(dci_temp_move1pt)

        # Compute sum of all MI
        MI_sum_temp = np.sum(selection_and_delta_move1pt[:, 1])

        # Keep track of all MI attempts with non-original grids
        delta_all_attempts = np.append(delta_all_attempts, delta_temp_move1pt)
        delta_temp = delta_temp_move1pt

        grid_temp = grid_temp_move1pt
        # sel_temp = sel_temp_move1pt
        [i0p, i1p, i2p] = pp.from_sel_to_grid_idxptr(sel_ordered_move1pt[-1], grid_temp)
        # print('i=', i, 'sel=', sel_ordered_move1pt, 'xyz=', grid_temp[i0p, i1p, i2p, :], 'MI_sum', MI_sum_temp)

        # CHECK for grid update opportunity, if the altered grid performed better MI then use that.
        if MI_sum_temp > MI_sum_k:
            # print("NEW optimal_grid: ", optimal_grid_values_temp)
            # print('*')  # print 'OK': for making a difference between bad and good attempts
            print(['OK: i=', i,
                   'sel=', [s for s in sel_ordered_move1pt],
                   'xyz=', [g for g in grid_temp[i0p, i1p, i2p, :]],
                   'MI_sum', MI_sum_temp], ',')

            MI_sum_k = MI_sum_temp
            # delta_updates = np.append(delta_updates, delta)
            sel_and_delta = selection_and_delta_move1pt
            _ = grid  # get rid of previous grid
            grid = np.copy(grid_temp)
            i += 1  # next attempt to improve grid

        else:  # iteration did not find coordinates with better MI
            print(['NO: i=', i,
                   'sel=', [s for s in sel_ordered_move1pt],
                   'xyz=', [g for g in grid_temp[i0p, i1p, i2p, :]],
                   'MI_sum', MI_sum_temp], ',')
            stuck_counter += 1
            print('stuck_counter=', stuck_counter, 'limit=', stuck_counter_limit)
            if stuck_counter_limit <= stuck_counter:
                break

    return grid, delta, sel_and_delta


###################################################################
# COVARIANCE GRAPH
###################################################################
def graph_cov(vgp,
        COVER_spatial,
        COVER_temp_pressure,
        SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z,
        # Dset_bkt_TS, COVER_bkt_time,
        dataset_pl,
        cell_timestep_num, boolcreate_cov_2, BETA_val, optimal_grid_values, office_data
    ):
    ''' Generate placement cov matrix: cov_vv
        - grid of spatial points 20 x 20
        - extend to 5d -> [x, y, z, t, p]
        - repeat 300 times
        - transformations as on training data
        - predict tracer
    -> 20x20x300 tracer values -> 300 needs to cover range of all temp, pressure and time values
    -> get cov_vv : 400x400x1
    '''
    # with tf.name_scope("VGP"):

    vgp.construct_tensorflow_graph()
    vgp.loss_function()
    vgp.load_saved_variational_model(office_data.num_inducing_points_)
    # Do not do vgp.loss_function() or .train_model() at this stage we just load a trained VGP and use it


    #===================================================================
    # COVARIANCE INIT
    #===================================================================
    print("CREATING COV -----")
    ts0 = tf.timestamp()

    with tf.name_scope("covariance"):
        with tf.name_scope("calc_cov_ij"):

            # ===================================================================
            # I. FUNCTIONS FOR WHILE LOOP
            # ===================================================================
            # REQ: from inside_node_calc_ij() cov_vv, last_cov_vv, xyz_cov_idxs must be seen 'implicitly',
            # thus stay being a variable, scatter_nd_update is possible

            with tf.name_scope("cov_init"):
                # ===================================================================
                # I. GLOBAL FOR WHILE LOOP
                # ===================================================================
                I0 = tf.constant(COVER_spatial[0], name="I0")
                I1 = tf.constant(COVER_spatial[1], name="I1")
                I2 = tf.constant(COVER_spatial[2], name="I2")

                cover_pt_const = tf.constant(COVER_temp_pressure)  # how many
                linsp_x = tf.linspace(-2., 2., COVER_spatial[0], name="linsp_x")
                linsp_y = tf.linspace(-2., 2., COVER_spatial[1], name="linsp_y")
                linsp_z = tf.linspace(-2., 2., COVER_spatial[2], name="linsp_z")
                linsp_x = tf.cast(linsp_x ,dtype=tf.float64)
                linsp_y = tf.cast(linsp_y, dtype=tf.float64)
                linsp_z = tf.cast(linsp_z, dtype=tf.float64)
                linsp_x = tf.reshape(linsp_x, [-1], name="reshape_")  # 1D I0 num values
                linsp_y = tf.reshape(linsp_y, [-1], name="reshape_")  # 1D I1 num values
                linsp_z = tf.reshape(linsp_z, [-1], name="reshape_")  # 1D I2 num values
                optimal_grid_xyz_var = tf.Variable(
                    initial_value=optimal_grid_values,  # sess.run(tf.global_variable_initializer())
                    shape=[COVER_spatial[0], COVER_spatial[1], COVER_spatial[2], 3]
                )  # optimal_grid_values, name="optimal_grid")

                #optimal_grid_xyz_var[i0, i1, i2, :] -> x, y, z
                # ===================================================================
                # linsp_t = tf.linspace(-3., 3., cover_pt_const, name="linsp_t")
                # linsp_p = tf.linspace(-3., 3., cover_pt_const, name="linsp_p")
                # T_, P_ = tf.meshgrid(linsp_t, linsp_p)
                # stack_tp = tf.stack([T_, P_], axis=2, name="grid_tp")
                # vec_tp = tf.reshape(stack_tp, [-1, 2], name="vec_tp")

                # ===================================================================
                # I. WORK ON THESE IN WHILE LOOP
                # ===================================================================
                cov_vv = tf.Variable(tf.zeros((I0 * I1 * I2, I0 * I1 * I2), dtype=tf.float64),
                                     dtype=tf.float64,
                                     name="cov_vv")
                last_cov_vv = tf.Variable(3.14,
                                          name="last_cov_vv",
                                          dtype=tf.float64)
                xyz_cov_idxs = tf.Variable(tf.zeros((I0 * I1 * I2, 3), dtype=tf.int32),
                                           dtype=tf.int32,
                                           name="xyz_cov_idxs")

            # ===================================================================
            # II. WHILE LOOP COV I J
            # ===================================================================

            # REQ: from inside_node_calc_ij() cov_vv, last_cov_vv, xyz_cov_idxs must be seen 'implicitly',
            # thus stay being a variable, scatter_nd_update is possible
            # -> 'cov_init' comes before (inside_node_calc_ij)
            #####################################################################################
            def local_kernel_filter(j_0, j_1, j_2, i_0, i_1, i_2, i, j):
                if boolcreate_cov_2  == True:
                    cov_vv_ij_, cov_vv_i_j_, ti, tj = inside_node_calc_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j)
                    cov_vv_ij, cov_vv_i_j = inside_node_store_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j, cov_vv_ij_, ti, tj)
                    return cov_vv_ij, cov_vv_i_j

                else:  # boolcreate_cov_2 == False:
                    beta = tf.constant(BETA_val, dtype=tf.float64, name="beta")  # or any other beta_small > 0
                    pi = tf.constant(np.pi, dtype=tf.float64, name="pi")
                    i_0 = tf.cast(i_0, tf.float64)
                    i_1 = tf.cast(i_1, tf.float64)
                    i_2 = tf.cast(i_2, tf.float64)
                    j_0 = tf.cast(j_0, tf.float64)
                    j_1 = tf.cast(j_1, tf.float64)
                    j_2 = tf.cast(j_2, tf.float64)
                    # delta_ij = tf.multiply(beta, tf.math.abs(tf.sqrt(
                    #                                                 tf.math.squared_difference(i_0, j_0) +
                    #                                                 tf.math.squared_difference(i_1, j_1) +
                    #                                                 tf.math.squared_difference(i_2, j_2))))  # distance in space not index?

                    delta_ij = tf.math.abs(tf.sqrt(
                        tf.math.squared_difference(i_0, j_0) +
                        tf.math.squared_difference(i_1, j_1) +
                        tf.math.squared_difference(i_2, j_2)))  # distance in space not index?

                    # def decay_fn_wrong(c_ij):
                    #     t_1 = tf.subtract(2*pi, delta_ij)
                    #     t_2 = 1 + tf.math.cos(delta_ij)/2
                    #     nominator = tf.multiply(t_1, t_2) + (3/2)*tf.math.sin(delta_ij)
                    #     return tf.abs(c_ij * nominator / 3*pi)

                    def decay_fn_(delta_ij_):
                        a = tf.cond(tf.less(delta_ij_,1.42), lambda: tf.cast(1.,tf.float64), lambda : tf.cast(0.,tf.float64))
                        return a

                    def decay_fn(delta_ij_):
                        decay = tf.math.exp(-tf.square(tf.multiply(beta, delta_ij_)) / (2 * pi))
                        a = tf.cond(tf.less(decay, 0.01), lambda: tf.cast(0.,tf.float64), lambda : tf.cast(decay, tf.float64))
                        return a

                    def calc_ij():
                        c_ij_, c_i_j_, ti, tj = inside_node_calc_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j)

                        c_ij_filt = decay_fn(delta_ij) * c_ij_
                        c_i_j_filt = decay_fn(delta_ij) * c_i_j_

                        c_ij, c_i_j = inside_node_store_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j, c_ij_filt, ti, tj)


                        print_cij = tf.print(["c_ij : ", c_ij, c_i_j], output_stream=sys.stdout, name='print_c_ij')
                        with tf.control_dependencies([print_cij]):
                            return c_ij, c_i_j

                    def zero_ij():
                        zero = tf.constant(0., tf.float64)
                        print_z = tf.print(["zero"], output_stream=sys.stdout, name='print_z')
                        with tf.control_dependencies([print_z]):
                            return zero, zero

                    cov_vv_ij, cov_vv_i_j = tf.cond(tf.less(delta_ij, 2*pi),
                                                    true_fn= calc_ij,
                                                    false_fn= zero_ij)
                    return cov_vv_ij, cov_vv_i_j

            def inside_node_calc_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j):

                aj_0 = tf.Assert(tf.less(tf.cast(j_0, tf.int32), I0), [j_0])
                aj_1 = tf.Assert(tf.less(tf.cast(j_1, tf.int32), I1), [j_1])
                aj_2 = tf.Assert(tf.less(tf.cast(j_2, tf.int32), I2), [j_2, I2])
                ai_0 = tf.Assert(tf.less(tf.cast(i_0, tf.int32), I0), [i_0])
                ai_1 = tf.Assert(tf.less(tf.cast(i_1, tf.int32), I1), [i_1])
                ai_2 = tf.Assert(tf.less(tf.cast(i_2, tf.int32), I2), [i_2, I2])
                ai = tf.Assert(tf.less(i, I0*I1*I2), [i])
                aj = tf.Assert(tf.less(j, I0*I1*I2), [j])

                # 1. collect xyz indices in while loop
                # 2. collect bucket_fraction_nums to create bkt indices -> bkt_x = (i_0/I0) // (1/SPLIT_bkt_x)
                # 3. vec_tp_i, vec_tp_j sampled across time from bkt in Dset_bkt_TS @ COVER_bkt_time density of values
                #    meaning the tracer values will be sampled across all time - > from a distribution across time ?


                def get_tracer_values(loc_xyz_i_, vec_tp_i_): # also j
                    # from i -> get tracers vector
                    # get XYZ index from indies,

                    vgp_xyztp = tf.cast(tf.expand_dims(
                        pp.graph_get_vgp_input_xyztp(loc_xyz_i_, vec_tp_i_), axis=0),
                        dtype=tf.float64)

                    assert (len(vgp_xyztp.shape) == 3)
                    assert vgp_xyztp.shape[0] == 1
                    assert vgp_xyztp.shape[2] == 5

                    vgp2 = tfd.VariationalGaussianProcess(
                        vgp.tf_graph.kernel,
                        index_points=vgp_xyztp,  #pred_idx_pts2,
                        inducing_index_points=vgp.tf_graph.inducing_index_points,
                        variational_inducing_observations_loc=vgp.tf_graph.variational_inducing_observations_loc_saved,
                        variational_inducing_observations_scale=vgp.tf_graph.variational_inducing_observations_scale_saved,
                        mean_fn=None,
                        observation_noise_variance=vgp.tf_graph.obs_noise_variance_softplus,
                        predictive_noise_variance=0.,
                        jitter=1e-6,  # 1e-6
                        validate_args=False
                    )

                    tracers_loc_i_ = tf.reshape(vgp2.mean(), shape=[-1])

                    # def vgp_output(np_vgp_xyztp_, np_tracers_loc_i_,
                    #                np_inducing_index_points, np_variational_loc, np_variational_scale):
                    #     if PRINTS:
                    #         print(' ')
                    #         print('vgp_xyztp: ', np_vgp_xyztp_[0,:3,:])
                    #         print('tracers_loc_i: ', np_tracers_loc_i_[:3])
                    #
                    #         print("inducing_index_points", np_inducing_index_points[0, :3, :])
                    #         print("variational_loc_saved", np_variational_loc[0, :3])
                    #         print("variational_scale_saved", np_variational_scale[0, 0, :3])
                    #         print(' ')
                    #     return np_vgp_xyztp, np_tracers_loc_i, np_inducing_index_points, np_variational_loc, np_variational_scale
                    #
                    # vgp_xyztp_, tracers_loc_i_, np_inducing_index_points_, np_variational_loc_, np_variational_scale_ \
                    #     = tf.numpy_function(vgp_output, [vgp_xyztp, tracers_loc_i_, inducing_index_points,
                    #                                      variational_inducing_observations_loc_saved,
                    #                                      variational_inducing_observations_scale_saved],
                    #                         [vgp_xyztp.dtype, tracers_loc_i_.dtype, inducing_index_points.dtype,
                    #                          variational_inducing_observations_loc_saved.dtype,
                    #                          variational_inducing_observations_scale_saved.dtype])

                    with tf.control_dependencies([tracers_loc_i_]):
                        return tracers_loc_i_  # tf.ones([300, 1], dtype=tf.float64)* tf.cast(pi_i, dtype=tf.float64)  # tracers_loc_i

                with tf.control_dependencies([aj_0, aj_1, aj_2, ai_0, ai_1, ai_2, ai, aj]):
                    # get_tracer_values_ = tf.function(get_tracer_values)

                    # loc_xyz_i = slice_grid_xyz(tf.cast(i_0, tf.int64), tf.cast(i_1, tf.int64), tf.cast(i_2, tf.int64), linsp_x, linsp_y, linsp_z)  # xyz values from linspace
                    # loc_xyz_j = slice_grid_xyz(tf.cast(j_0, tf.int64), tf.cast(j_1, tf.int64), tf.cast(j_2, tf.int64), linsp_x, linsp_y, linsp_z)  # xyz values from linspace
                    loc_xyz_i = optimal_grid_xyz_var[i_0, i_1, i_2, :]
                    loc_xyz_j = optimal_grid_xyz_var[j_0, j_1, j_2, :]

                    # loc_xyz_i = slice_grid_xyz_var(tf.cast(i_0, tf.int64), tf.cast(i_1, tf.int64), tf.cast(i_2, tf.int64),
                    #                                optimal_grid_xyz_var)#, linsp_y, linsp_z)  # xyz values from linspace
                    # loc_xyz_j = slice_grid_xyz_var(tf.cast(j_0, tf.int64), tf.cast(j_1, tf.int64), tf.cast(j_2, tf.int64),
                    #                                optimal_grid_xyz_var)#linsp_x, linsp_y, linsp_z)  # xyz values from linspace

                cell_id = get_cell_id(i_0, i_1, i_2, I0, I1, I2, SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z)
                vec_tp_i_uniform = sample_uniform()  # OPTION 1.- SAMPLE TP UNIFORM
                vec_tp_i_distribution = sample_cell(dataset_pl, cell_id, cell_timestep_num) # OPTION 2.- SAMPLE TIMERANGE TP HISTOGRAM

                tracers_loc_i = get_tracer_values(loc_xyz_i, vec_tp_i_distribution)
                tracers_loc_j = get_tracer_values(loc_xyz_j, vec_tp_i_distribution)

                pr_op_tr = tf.print(['tracers_loc_i, j=', tracers_loc_i, tracers_loc_j],
                                    output_stream=sys.stdout,
                                    name='print_tracers_loc_i_j')

                # def numpy_fun_tracer(tracers_loc_i, tracers_loc_j):
                #     if PRINTS or 1:
                #         print(' ')
                #         print('tracers_loc_i: ', tracers_loc_i[:5])
                #         print('tracers_loc_j: ', tracers_loc_j[:5])
                #         print(' ')
                #     return tracers_loc_i, tracers_loc_j
                #
                # [tracers_loc_i, tracers_loc_j] = tf.numpy_function(numpy_fun_tracer, [tracers_loc_i, tracers_loc_j], [tracers_loc_i.dtype, tracers_loc_j.dtype])

                with tf.control_dependencies([tracers_loc_i, tracers_loc_j]):

                    # tr_mean = tf.constant([0.0018159087825037148], dtype=tf.float64)
                    # tr_stdev = tf.constant([0.0007434639347162126*3000000], dtype=tf.float64)

                    tr_mean = tf.constant([0], dtype=tf.float64)
                    tr_stdev = tf.constant([1], dtype=tf.float64)

                    t_i_ = tracers_loc_i - tr_mean
                    t_j_ = tracers_loc_j - tr_mean

                    t_i = t_i_ / tr_stdev
                    t_j = t_j_ / tr_stdev
                    cov_vv_ij = tfp.stats.covariance(t_i, t_j, sample_axis=0, event_axis=None)
                    cov_vv_ij_pr = tf.print(['cov_vv_ij=', cov_vv_ij])
                    return cov_vv_ij, cov_vv[i, j], t_i, t_j

                    # DEBUG:
                    # def numpy_fun(ti, tj, cov_vvij):
                    #     if PRINTS:
                    #         print(' ')
                    #         print('ti: ', ti[:5])
                    #         print('tj: ', tj[:5])
                    #         print('cov_vvij: ', cov_vvij)
                    #         print(' ')
                    #     return ti, tj, cov_vvij
                    #
                    # [t_i, t_j, cov_vv_ij] = tf.numpy_function(numpy_fun, [t_i, t_j, cov_vv_ij], [t_i.dtype, t_j.dtype, cov_vv_ij.dtype])

            def inside_node_store_ij(j_0, j_1, j_2, i_0, i_1, i_2, i, j, cov_vv_ij, t_i, t_j):

                # with tf.control_dependencies([t_i, t_j, cov_vv_ij
                #   , cov_vv_ij_pr
                # ]):
                update = tf.reshape(cov_vv_ij, [], name="update")

                indexes1 = tf.cast([i, j], dtype=tf.int32, name='indexes')
                indexes2 = tf.cast([j, i], dtype=tf.int32, name='indexes')

                op1 = tf.scatter_nd_update(cov_vv, [indexes1], [update])  # [[i0, 0]], [3.+i0+0])
                op2 = tf.scatter_nd_update(cov_vv, [indexes2], [update])  # [[i0, 0]], [3.+i0+0])
                op3 = tf.assign(last_cov_vv, update)  # test assignment is working

                pr_op = op3
                # pr_op = tf.print(['inside', i, j, cov_vv[i, j], update],
                #                  output_stream=sys.stdout,
                #                  name='print_inside_cov_vv_i_j_update')

                with tf.control_dependencies([op1, op2, op3, pr_op]):
                    # update a row of x y z
                    indexes_x = tf.cast([j, 0], dtype=tf.int32,name='indexes_x')
                    indexes_y = tf.cast([j, 1], dtype=tf.int32,name='indexes_y')
                    indexes_z = tf.cast([j, 2], dtype=tf.int32,name='indexes_z')

                    # get samples
                    # def calc_uniform_index():
                    update_idx_x = tf.reshape(j_0, [], name="update_x")
                    update_idx_y = tf.reshape(j_1, [], name="update_y")
                    update_idx_z = tf.reshape(j_2, [], name="update_z")
                    # return update_idx_x_, update_idx_y_, update_idx_z_

                    # update_idx_x, update_idx_y, update_idx_z = calc_bucket_index(j_0, j_1, j_2, i)

                    # xyz_cov_idxs - SAVE xyz to be remapped to after algorithm 2
                    op4 = tf.scatter_nd_update(xyz_cov_idxs, [indexes_x], [update_idx_x])
                    op5 = tf.scatter_nd_update(xyz_cov_idxs, [indexes_y], [update_idx_y])
                    op6 = tf.scatter_nd_update(xyz_cov_idxs, [indexes_z], [update_idx_z])

                    with tf.control_dependencies([op1, op2, op3, op4, op5, op6, cov_vv_ij]):
                        return cov_vv_ij, cov_vv[i, j]  # END inside_node_store_ij

            def get_cell_id(i_0, i_1, i_2, I0, I1, I2, SPLIT_cell_x, SPLIT_cell_y,
                            SPLIT_cell_z):  # j_0, j_1, j_2, i):
                # calculate bucket number and return it.
                i_0_ = tf.cast(i_0, tf.float64)
                i_1_ = tf.cast(i_1, tf.float64)
                i_2_ = tf.cast(i_2, tf.float64)
                I0_ = tf.cast(I0, tf.float64)
                I1_ = tf.cast(I1, tf.float64)
                I2_ = tf.cast(I2, tf.float64)

                cell_x_i = SPLIT_cell_x * i_0_ // I0_  # (i_0_ / I0_) // (1 / SPLIT_cell_x)  # tf.floordiv(tf.math.divide(i_0, I0), tf.math.divide(1, SPLIT_bkt_x))
                cell_y_i = SPLIT_cell_y * i_1_ // I1_  # (i_1_ / I1_) // (1 / SPLIT_cell_y)
                cell_z_i = SPLIT_cell_z * i_2_ // I2_  # (i_2_ / I2_) // (1 / SPLIT_cell_z)
                cell_id = cell_x_i + (cell_y_i * SPLIT_cell_x) + (cell_z_i * SPLIT_cell_x * SPLIT_cell_y)
                return cell_id

            def sample_cell(dataset_pl, cell_id, cell_timestep_num):
                # takes a constant to make shapes of placeholders: timesteps_in_cell, cell_id and dataset

                # Get all timesteps from cell: cell_id
                cell_rows_selection = tf.slice(dataset_pl,
                                               begin=[tf.cast(cell_id * cell_timestep_num, tf.int64), 0],
                                               size=[cell_timestep_num, dataset_pl.shape[1]],
                                               name="cell_ts_pl")

                op1 = tf.Assert(tf.equal(cell_rows_selection[0, 0], cell_id),
                                [cell_rows_selection[0, :], cell_timestep_num, cell_id])
                op2 = tf.Assert(tf.equal(cell_rows_selection[-1, 0], cell_id),
                                [cell_rows_selection[0, :], cell_timestep_num, cell_id])

                # Get temp_ave and pressure_ave columns from cell_selection
                cell_selection = cell_rows_selection[ : ,2:4]
                with tf.control_dependencies([op1, op2]):
                    return cell_selection

            def sample_uniform():
                linsp_t = tf.linspace(-3., 3., cover_pt_const, name="linsp_t")
                linsp_p = tf.linspace(-3., 3., cover_pt_const, name="linsp_p")
                T_, P_ = tf.meshgrid(linsp_t, linsp_p)
                stack_tp = tf.stack([T_, P_], axis=2, name="grid_tp")
                vec_tp = tf.reshape(stack_tp, [-1, 2], name="vec_tp")
                vec_tp_sample = vec_tp
                return vec_tp_sample


        with tf.name_scope("cov_calc"):
            cond0 = lambda i_0, I0_: tf.less(i_0, I0_)
            def body0(i_0, I0_):
                cond1 = lambda i_1, I1_: tf.less(i_1, I1_)

                def body1(i_1, I1_):
                    cond2 = lambda i_2, I2_: tf.less(i_2, I2_)

                    def body2(i_2, I2_):
                        cond_0 = lambda j_0, J0_: tf.less(j_0, J0_)

                        # we'd like to know the index=i of a position (i_0, i_1, i_2)
                        # I can't remember why did we go inside out, TODO: write it here
                        i_strde0 = I2_ * I1_
                        i_strde1 = I2_
                        i_strde2 = 1

                        # if i_0 = 0..2, i1=0..2, i_2=0, i_strde0 or i_strde1 have to be 1 (and i_strde1 = I2_ == 1)
                        i_mult0 = tf.multiply(i_0, i_strde0)
                        i_mult1 = tf.multiply(i_1, i_strde1)
                        i_mult2 = tf.multiply(i_2, i_strde2)
                        i_add = tf.add(i_mult2, i_mult1)
                        i_ = tf.add(i_mult0, i_add)
                        ai_ = tf.Assert(tf.less(i_, I0 * I1 * I2), [i_, i_strde0, i_strde1, i_strde2,
                                                                    i_mult0, i_mult1, i_mult2,
                                                                    I0_, I1_, I2_])
                        # for long running cov_vv generation, let's see the progress
                        pr_i = tf.print(['i =', i_, tf.timestamp()-ts0],
                                        # output_stream=sys.stdout,
                                        name='print_tracers_loc_i_j')

                        # it is repeated below the functions: (this may help to see the progress on a bigger cov_vv)
                        with tf.control_dependencies([i_,
                                                      # pr_i,
                                                      ai_]):  # does not work here: control_dependencies: pr_i
                            i = i_

                        def body_0(j_0, J0_):
                            cond_1 = lambda j_1, J1_: tf.less(j_1, J1_)

                            def body_1(j_1, J1_):
                                def cond_2(j_2, J2_):
                                    j_strde0 = J2_ * J1_
                                    j_strde1 = J2_
                                    j_strde2 = 1

                                    j_mult0 = tf.multiply(j_0, j_strde0)
                                    j_mult1 = tf.multiply(j_1, j_strde1)
                                    j_mult2 = tf.multiply(j_2, j_strde2)
                                    j_add = tf.add(j_mult2, j_mult1)
                                    j_ = tf.add(j_mult0, j_add)
                                    aj_ = tf.Assert(tf.less(j_, I0 * I1 * I2), [j_, j_strde0, j_strde1, j_strde2,
                                                                                j_mult0, j_mult1, j_mult2,
                                                                                J0_, J1_, J2_])
                                    with tf.control_dependencies([aj_]):
                                        j = j_

                                    return tf.math.logical_and(tf.less(j_2, J2_), tf.less_equal(j, i))

                                def body_2(j_2, J2_):
                                    # we'd like to know the index=j of a position (j_0, j_1, j_2)
                                    j_strde0 = J2_ * J1_
                                    j_strde1 = J2_
                                    j_strde2 = 1

                                    j_mult0 = tf.multiply(j_0, j_strde0)
                                    j_mult1 = tf.multiply(j_1, j_strde1)
                                    j_mult2 = tf.multiply(j_2, j_strde2)
                                    j_add = tf.add(j_mult2, j_mult1)
                                    j_ = tf.add(j_mult0, j_add)
                                    a2j_ = tf.Assert(tf.less(j_, I0 * I1 * I2), [j_, j_strde0, j_strde1, j_strde2,
                                                                                 j_mult0, j_mult1, j_mult2,
                                                                                 J0_, J1_, J2_])
                                    with tf.control_dependencies([a2j_]):
                                        j = j_

                                    # j = tf_print2(j, [i, j], message='i, j = ')

                                    #####################################################################################
                                    cov_vv_ij, cov_vv_i_j = local_kernel_filter(j_0, j_1, j_2, i_0, i_1, i_2, i, j)
                                    #####################################################################################

                                    print_op = cov_vv_ij
                                    #
                                    # For some reason I don't know, cov_vv[i, j] seems to be 0 here, even if
                                    #  tf.print shows the updates inside ..
                                    # print_op = tf.print(['loop: ', i, j, cov_vv[i, j], cov_vv_ij, cov_vv_i_j],
                                    #                     output_stream=sys.stdout,
                                    #                     name='print_cov_vv_i_j_cov_vv_ij_2x')

                                    with tf.control_dependencies([cov_vv_ij, cov_vv_i_j, print_op]):
                                        j_2_next = tf.add(j_2, 1)
                                    return [j_2_next, J2_]

                                [loop_j2, _] = tf.while_loop(cond_2, body_2, loop_vars=[0, I2], name="loop_j2")
                                with tf.control_dependencies([loop_j2]):
                                    j_1_next = tf.add(j_1, 1)
                                return [j_1_next, J1_]  # end body_1

                            [loop_j1, _] = tf.while_loop(cond_1, body_1, loop_vars=[0, I1], name="loop_j1")
                            with tf.control_dependencies([loop_j1]):
                                j_0_next = tf.add(j_0, 1)
                            return [j_0_next, J0_]  # end body_0

                        with tf.control_dependencies([i_,
                                                      # pr_i,
                                                      ai_]):
                            [loop_j0, _] = tf.while_loop(cond_0, body_0, loop_vars=[0, I0], name="loop_j0")

                        with tf.control_dependencies([loop_j0]):
                            i_2_next = tf.add(i_2, 1)
                        return [i_2_next, I2_]  # end body2

                    [loop_i2, _] = tf.while_loop(cond2, body2, loop_vars=[0, I2], name="loop_i2")
                    with tf.control_dependencies([loop_i2]):
                        i_1_next = tf.add(i_1, 1)
                    return [i_1_next, I1_]  # end body1

                [loop_i1, _] = tf.while_loop(cond1, body1, loop_vars=[0, I1], name="loop_i1")
                with tf.control_dependencies([loop_i1]):
                    i_0_next = tf.add(i_0, 1)
                return [i_0_next, I0_]  # end body0

            [while_i0_idx, while_i0_end] = tf.while_loop(cond0, body0, loop_vars=[0, I0], name="loop_i0")

            # NOTE: TF does not record order in which operations are created just their dependencies,
            # use control_dependecies when order of execution in graph matters'
            # this is to make sure variables (stateful objects) are computed in correct order/avoid ambiguity.
            # Assignment is delayed until values in list have been computed
            with tf.control_dependencies([while_i0_idx, while_i0_end]):
                return cov_vv, \
                       last_cov_vv, \
                       while_i0_idx, \
                       while_i0_end, \
                       xyz_cov_idxs


# ========================================================================
# Final k = 7 placement with optimal grid
# ========================================================================
def place_all_sensors(office_data, current_grid, vgp):
    s_time = time.time()
    office_data.indirection.K_SENSORS = 7
    _, _, optimal_selection_and_delta = RUN_cov_generation(office_data, current_grid, vgp)
    optimal_selection = optimal_selection_and_delta[:, 0]
    deltas = optimal_selection_and_delta[:, 1]
    MI_sum = np.sum(optimal_selection_and_delta[:, 1])
    vtk_IDs = pp.from_selection_in_grid_to_ID(optimal_selection, current_grid)
    e_time = time.time()
    d_time = e_time - s_time
    d_time_str = str(d_time)

    if office_data.indirection.GEN_LOCAL_kernel == True:
        alg_used = "alg3"
    else:
        alg_used = "alg2"
    loc_it = 'main_tests/test_MI/it/'
    loc_da = 'main_tests/test_MI/da/'
    c = str(office_data.indirection.COVER_spatial[0])+'x' + \
        str(office_data.indirection.COVER_spatial[1])+'x' + \
        str(office_data.indirection.COVER_spatial[2])
    s = str(office_data.indirection.K_to_optimize)
    h = str(office_data.indirection.Kth)
    tag = 'GRID' + c + '_OPT' + s + '_AT' + h + '_USING' + alg_used

    it_results = pd.DataFrame(data={
            'selection': optimal_selection,
            'vtk_ID': vtk_IDs,
            'deltas': deltas,
            'MI_sum': MI_sum,
            'times': d_time_str})

    folder = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'artifacts/data/placement-results', tag)
    os.makedirs(folder, exist_ok=True)
    res = os.path.join(folder, f'RESULTSit{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.csv')
    it_results.to_csv(res)

    VALIDATE_PLACEMENT = False
    if VALIDATE_PLACEMENT:
        background_error, DA_error = va.validate_placement(vtk_IDs)
        da_results = pd.DataFrame(data={
            'selection': [optimal_selection],
            'vtk_ID': [vtk_IDs],
            'deltas': [deltas],
            'MI_sum': MI_sum,
            'times': d_time_str,
            'background_error': background_error,
            'DA_error': DA_error})
        da_results.to_csv(loc_da + tag + '_RESULTSda.csv')


def RUN_cov_generation(office_data, optimal_grid_values, vgp):
    # SAVE_CACHE_original = 'main_datasets_/placement_algorithm_cache.csv'

    SET_ID = 3
    CSV_CELL = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'artifacts/data/dataset_2_cov', 'dataset_2_cov_' + str(SET_ID) + '.csv')
    split_cell_x, split_cell_y, split_cell_z = 4, 4, 4  # SPLIT_cell_ property of file generation

    # COVER_spatial_original = [25, 25, 1]  # 70  # SPATIAL_COVER = 7  also size of covariance matrix in the end
    cover_spatial, file_cov_vv, file_cov_idxs, \
    file_delt_ch_it, SAVE_SELECTION, GEN_LOCAL_kernel, BETA_val \
        = office_data.get_spatial_splits()

    RUN_ALGO_2 = not GEN_LOCAL_kernel

    SAVE_CACHE = file_delt_ch_it
    SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z = split_cell_x, split_cell_y, split_cell_z
    COVER_spatial = cover_spatial
    boolcreate_cov_2 = RUN_ALGO_2

    tf.reset_default_graph()

    # Files #
    LOGDIR = "./log_dir_/"+datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H:%M:%S')+"_vgp"
    # CSV_COV = 'main_datasets_/Dset_bkt_TS_small_600.csv'
    # df_COV = pd.read_csv(CSV_COV, encoding='utf-8', engine='c')
    # df_COV.drop(columns=['Unnamed: 0'], inplace=True)

    # CSV_GP = 'main_datasets_/Dset_xyz_ave_small.csv'
    # df_GP = pd.read_csv(CSV_GP, encoding='utf-8', engine='c')
    # df_GP.drop(columns=['Unnamed: 0'], inplace=True)

    #
    # here we select a dataset that will represent the distribution of the pressure, temperature values
    #
    # SET_ID = 2
    # CSV_2_CELL = './dataset_2_cov/dataset_2_cov_' + str(SET_ID) + '.csv'
    df_cell = pd.read_csv(CSV_CELL, encoding='utf-8', engine='c')
    df_cell.drop(columns=['Unnamed: 0'], inplace=True)

    # normalize Temperature_ave, Pressure_ave, so the input is scaled to be used by the VGP
    eTempCol, ePressCol = 3, 4
    COLUMNS_MEAN, COLUMNS_STDEV, tracer_MEAN, tracer_STDEV = office_data.norm.get_normalizing_parameters()
    df_cell.loc[:, 'Temperature_ave'] -= COLUMNS_MEAN[eTempCol]
    df_cell.loc[:, 'Temperature_ave'] /= COLUMNS_STDEV[eTempCol]
    df_cell.loc[:, 'Pressure_ave'] -= COLUMNS_MEAN[ePressCol]
    df_cell.loc[:, 'Pressure_ave'] /= COLUMNS_STDEV[ePressCol]
    # now the input is scaled to be used by the VGP
    # print(df_cell.loc[0, 'Temperature_ave'])
    # print(df_cell.loc[0, 'Temperature_ave'])
    # print(df_cell.loc[0, 'Temperature_ave'])

    def fill_missing_cell_df(df_cell):
        MAXCELLS = SPLIT_cell_x * SPLIT_cell_y * SPLIT_cell_z

        df_corrected = df_cell.loc[df_cell['Bucket'] == 0, :]

        assert len(df_corrected) == 250
        prev_filter = None

        for i in range(1, MAXCELLS):
            df_cell_filter = df_cell.loc[df_cell['Bucket'] == i, :]
            if len(df_cell_filter) == 250:
                df_corrected = df_corrected.append(df_cell_filter)
                prev_filter = df_cell_filter

            else:  # only works if there is a Cell 0.
                # prev_filter.loc[:, prev_filter.columns.get_loc('Bucket')] = i
                to_append = prev_filter.copy()
                to_append.loc[:, 'Bucket'] = i
                df_corrected = df_corrected.append(to_append)

        df_corrected = df_corrected.reset_index(drop=True)
        return df_corrected

    df_cell = fill_missing_cell_df(df_cell)
    dataset_shape = df_cell.shape
    dataset_pl = tf.constant(df_cell.to_numpy(), tf.float64)
    cell_timestep_num = 250

    # sample_uniform() used cover_pt_const==COVER_temp_pressure
    # we are using sample_cell(, , cell_timestep_num == 250) now (which maybe is more than enough ..)
    COVER_temp_pressure = 12  # Unused variable!

    # COVER_bkt_time = len(df_COV)
    # num_training_points_, num_predictive_index_points_one_dir, \
    # num_predictive_index_points_,num_inducing_points_, \
    # feature_dims\
    #     = office_data.get_VGP_parameters_from()  # last VGP fitted

    # ===================================================================
    # II.1 - COV calc
    # ===================================================================
    [cov_vv_r, last_cov_vv_r,
    while_i0_idx,
    while_i0_end,
    xyz_cov_idxs_r] \
        = graph_cov(vgp,
                    COVER_spatial, COVER_temp_pressure,
                    SPLIT_cell_x, SPLIT_cell_y, SPLIT_cell_z,
                    dataset_pl,
                    cell_timestep_num,
                    boolcreate_cov_2,
                    BETA_val,
                    optimal_grid_values,
                    office_data,
                    )

    # ===================================================================
    # II.2 - PLACEMENT
    [selection_idxs_A, _, delta_cached_iters_tensor, A_selection_and_delta]  \
        = mi.sparse_placement_algorithm_2(cov_vv_r, office_data.indirection.K_SENSORS, COVER_spatial)
    # ===================================================================
    # with tf.control_dependencies([cov_vv_r, while_i0_idx, while_i0_end]):

    # ===================================================================
    # III.3 - PLACEMENT-algo-3
    # ===================================================================
    # with tf.control_dependencies([cov_vv_r, while_i0_idx, while_i0_end]):
    # cutoff =
    if office_data.indirection.GEN_LOCAL_kernel:
        selection_idxs_A_algo3, _, delta_cached_iters_tensor_algo3 \
            = mi.sparse_placement_algorithm_3(cov_vv_r, office_data.indirection.K_SENSORS, COVER_spatial, office_data.indirection['cutoff'])

    # ===================================================================
    # GRAPH SAVER
    # ===================================================================
    print("LOGDIR", LOGDIR)
    summ = tf.summary.merge_all()
    writer = tf.summary.FileWriter(LOGDIR)
    saver = tf.train.Saver()  # all the object in the graph mapped

    # ===================================================================
    # GRAPH CALLS
    # ===================================================================
    model_load_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'artifacts/checkpoints/vgp-office/saved_variables_dataset_1_gp')
    with tf.Session() as sess:
        tfkb.set_session(sess)  # keras backend joined
        sess.run(tf.global_variables_initializer())  # after the init
        writer.add_graph(sess.graph)
        vgp.saver_VGP.restore(sess, model_load_path)

        [loc_set_op_, scale_set_op_] = sess.run([vgp.tf_graph.variational_inducing_observations_loc_saved,
                                                 vgp.tf_graph.variational_inducing_observations_scale_saved])
        [variational_loc_v, variational_scale_v] = sess.run([vgp.tf_graph.variational_loc,
                                                             vgp.tf_graph.variational_scale])

        print('loc_set_op_: ', loc_set_op_[0, :5])
        print("variational_loc_v", variational_loc_v[0, :5])
        print('scale_set_op_: ', scale_set_op_[0, :1, :5])
        print("variational_scale_v", variational_scale_v[0, :1, :5])

        #===================================================================
        # SESS
        #===================================================================
        [w0, w1, cov_vv_] = sess.run([while_i0_idx, while_i0_end, cov_vv_r])  #, feed_dict={values: df_COV})
        # elast = sess.run(last_cov_vv_r, feed_dict={values: df_COV})

        print("cov_vv[0, :w1], after running while_op")
        print('w0=', w0, 'w1=', w1)
        # print(cov_vv_[:, :])

        _, file_cov_vv, file_cov_idxs, _, _, use_localkernel, beta = office_data.get_spatial_splits()  # get second time, now for algorithm purpose
        pd.DataFrame(cov_vv_).to_csv(file_cov_vv)

        xyz_cov_idxs_ = sess.run(xyz_cov_idxs_r)  #, feed_dict={values: df_COV})
        pd.DataFrame(xyz_cov_idxs_).to_csv(file_cov_idxs)

        if use_localkernel:
            [selection_idxs_, delta_cached_its_] = sess.run([selection_idxs_A_algo3, delta_cached_iters_tensor_algo3])

        else:
            # Run selection algorithm once:
            [selection_idxs_, delta_cached_its_, A_selection_and_delta_] = sess.run(
                [selection_idxs_A, delta_cached_iters_tensor, A_selection_and_delta])

        # xyz_selection = xyz_cov_idxs_[selection_idxs_]
        print("selection_idxs_", selection_idxs_)
        # print(xyz_cov_idxs_)

        pd.DataFrame(delta_cached_its_).to_csv(SAVE_CACHE)
        pd.DataFrame(selection_idxs_).to_csv(SAVE_SELECTION)

        # fig_rows, fig_cols = 3, 4
        # fig, ax = plt.subplots(fig_rows, fig_cols,
        #                        figsize=(15, 15),
        #                        squeeze=True,  # use just one index: ax[i]
        #                        constrained_layout=True)
        return selection_idxs_.values, delta_cached_its_, A_selection_and_delta_


# ========================================================================
# Full pass
# ========================================================================
def full_pass_with_wrapper(
        COVER,
        K_to_optimize,
        GEN_LOCAL_kernel,
        BETA_val,
        CONFIG,
        GRID_RANGE_x=[-2., 2.],
        GRID_RANGE_y=[-2., 2.],
        GRID_RANGE_z=[-2., 2.],
        single_pass=False
):
    start_time = time.time()
    office_data = pp.VGPCaseStudyOfficeData(CONFIG)
    office_data.indirection.COVER_spatial = COVER
    office_data.indirection.K_to_optimize = K_to_optimize
    office_data.indirection.GRID_RANGE_x = GRID_RANGE_x
    office_data.indirection.GRID_RANGE_y = GRID_RANGE_y
    office_data.indirection.GRID_RANGE_z = GRID_RANGE_z
    office_data.indirection.GEN_LOCAL_kernel = GEN_LOCAL_kernel
    office_data.indirection.BETA_val = BETA_val
    office_data.inducing_index_points = office_data.generate_predictive_index_points(office_data.num_inducing_points_)
    office_data.get_idx_obs_coord_range()
    original_grid = pp.init_grid(office_data.indirection)
    if (office_data.indirection.COVER_spatial[0] *
        office_data.indirection.COVER_spatial[1] *
        office_data.indirection.COVER_spatial[2]) \
            < office_data.indirection.K_SENSORS:
        print("has to be >= K_SENSORS")
        assert False

    vgp = mvgp.VGP.VGPBuilder(office_data.get_VGP_parameters(), office_data.config_path)
    # ///////////////////////////////////////////////////////////////////////
    RUN_SHORT_TESTS = False
    if RUN_SHORT_TESTS:
        tmp.TEST_cov_2_equal_cov_3(office_data.indirection, original_grid)

    RUN_ORIGINAL = single_pass
    if RUN_ORIGINAL:
        _, _, _ = RUN_cov_generation(vgp, office_data, original_grid)

    RUN_GRID_MODIFICATION = not single_pass
    if RUN_GRID_MODIFICATION:

        # ========================================================================
        # vtk test Original k=7 placement with original grid
        TEST_selection = False
        if TEST_selection:
            _, _, original_selection_and_delta = RUN_cov_generation(office_data, original_grid, vgp)
            original_selection = original_selection_and_delta[:, 0]
            test_selection = [0, 3, 6, 12, 15, 20, 24]  # [[-2,2,0], [-1,1,0], [0,0,0], [1,1,0], [1.5, 0.7,0], [2,2,0], [0.5, 0.5,0]]
            vtk_ID_test = pp.from_selection_in_grid_to_ID(test_selection, original_grid)
            print("original_grid: ", original_grid)

        # ========================================================================
        # Original k=7 placement with original grid
        RUN_ORIGINAL_FIRST = True
        if RUN_ORIGINAL_FIRST:
            office_data.indirection.Kth = 0  # original case
            office_data.indirection.K_SENSORS = 7  # TODO: is this different from 7 == K_to_optimize,
            #  because in the while below we set that to
            #  indirection['K_SENSORS'] = indirection['Kth'] (..here == 0)
            place_all_sensors(office_data, original_grid, vgp)
        office_data.indirection.Kth = 1

        # vtk_ID_original = pp.from_selection_in_grid_to_ID(original_selection, original_grid)
        # ========================================================================
        #  Optimize grid
        # after each gridpoint optimization, the results are saved for plots
        # K_to_optimize specifies how many k will be run and optimized for.
        # GLOBALS for grid optimization --------------------------------------------
        optimal_grid = original_grid
        range_diff = (office_data.indirection.GRID_RANGE[1] - office_data.indirection.GRID_RANGE[0])
        one_grid_unit = (range_diff / (office_data.indirection.COVER_spatial[0]))
        EPSILON = one_grid_unit / office_data.indirection.EPSILON_fraction
        attempt_num = office_data.indirection.OPTIMIZATION_attempts

        # ========================================================================
        # Optimize then check result for 7 placement
        # ========================================================================
        while office_data.indirection.Kth <= office_data.indirection.K_to_optimize:

            # OPTIMIZE ============================================
            office_data.indirection.K_SENSORS = office_data.indirection.Kth
            optimal_grid, optimal_delta1, sel_and_delta_original1 \
                = optimize_grid_for_last_k(office_data, optimal_grid, attempt_num, EPSILON, vgp)

            # TEST ================================================
            office_data.indirection.K_SENSORS = 7  # reset to see how optimization did for 7 sensor placements
            place_all_sensors(office_data, optimal_grid, vgp)
            office_data.indirection.Kth += 1


        # ========================================================================
         # walk_limit = one_grid_unit / (2/3)
        # k = 2 optimize --------------------------------------------------------
        if office_data.indirection.Kth <= office_data.indirection.K_to_optimize:
            office_data.indirection.K_SENSORS = 2
            optimal_grid, optimal_delta2, sel_and_delta_optimal2 = optimize_grid_for_last_k(office_data, optimal_grid, attempt_num, EPSILON, vgp)
            office_data.indirection.K_SENSORS = 7  # reset
            place_all_sensors(office_data, optimal_grid, vgp)
            office_data.indirection.Kth += 1
        # # k = 5 optimize --------------------------------------------------------
        # if indirection['Kth'] <= indirection['K_to_optimize']:
        #     indirection['K_SENSORS'] += 1
        #     optimal_grid, optimal_delta5, sel_and_delta_optimal5 = optimize_grid_for_last_k(indirection, optimal_grid, attempt_num, EPSILON)
        #     indirection['K_SENSORS'] = 7 # reset
        #     place_all_sensors(indirection, optimal_grid)
        #     indirection['Kth'] += 1

        # EPSILON /= 2
        # optimal_grid, optimal_delta1 = optimize_grid_for_last_k(indirection, optimal_grid, 4, EPSILON)
        # print("ORIGINAL GRID", original_grid)
        # print("OPTIMAL GRID", optimal_grid)

    end_time = time.time()
    diff_time = end_time - start_time
    diff_time_str = str(diff_time)
    print('placement time: ', diff_time_str)


if __name__ == '__main__':
    CONFIG = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'artifacts/config', 'vgp-office-6d.yaml')
    full_pass_with_wrapper(COVER=[3, 3, 1], K_to_optimize=2, GEN_LOCAL_kernel=False, BETA_val=None, CONFIG=CONFIG, GRID_RANGE_z=[0., 0.1])
    #full_pass_with_wrapper(COVER=[9, 9, 1], K_to_optimize=7, GEN_LOCAL_kernel=False, BETA_val=None, CONFIG=CONFIG, GRID_RANGE_x=[-0.9, 0.9], GRID_RANGE_y=[-0.9, 0.9], GRID_RANGE_z=[-1.3, 0.])

import tensorflow as tf
if tf.__version__[0] == '2':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

import numpy as np
import sys


def delta_cached_uptodate_empty(N):
    delta_cached_uptodate = tf.Variable(lambda: tf.fill([N, 1], False), shape=[N, 1])
    return delta_cached_uptodate


def dg_create_random_cov(n):
    matrix_rand = np.random.uniform(0,1,n**2).reshape(-1,n)
    cov_vv = np.dot(matrix_rand, matrix_rand.T)
    return cov_vv


def call_pinv(a):
    assert a.shape[0] == a.shape[1]
    if a.shape[0] == 1:
        return 1 / a
    else:
        r = np.linalg.pinv(a)
        return r


def argmax_cache_linear(cache, A, V):
    y_st = -1  # index of coordinate
    delta_st = -1
    for y in V:
        if y in A:  # if y in A then don't consider: continue
            continue

        delta_y = cache[y]

        if delta_st < delta_y:
            delta_st = delta_y  # update largest delta yet, delta_star
            y_st = y  # update index

    # now we have considered all y-s in the cache, and the winner is y_st
    return y_st


def sparse_argmax_cache_linear(cache, A, V):
    # y_st = -1  # index of coordinate
    # delta_st = -1

    V_minus_A = tf.sets.difference(V, A)
    delta_y_s = tf.reshape(tf.gather_nd(
        tf.reshape(cache, [-1, 1]),
        tf.reshape(V_minus_A.indices[:, 0], [-1, 1])
    ), [-1])
    delta_st = tf.reduce_max(delta_y_s)  # max
    val_idx = tf.where(tf.equal(delta_y_s, delta_st)) # index of max
    # # val_col = 0
    # # index_col = 1
    idx_st = val_idx[0, 0]  # get index of delta_st
    y_st = V_minus_A.values[idx_st]

    # y_st = snippets.tf_print2(y_st,
    #             [cache,
    #              V_minus_A.values,
    #              delta_y_s,
    #              delta_st,
    #              val_idx, idx_st,
    #              y_st],
    #             'sparse_argmax_cache_linear')

    # now we have considered all y-s in the cache, and the winner is y_st
    return y_st


def make_slice(cov_vv, y, A):
    cov_yA = np.zeros(shape=[len(y), len(A)])
    for i in range(cov_yA.shape[0]):
        for j in range(cov_yA.shape[1]):
            cov_yA[i,j] = cov_vv[y[i], A[j]]
    return cov_yA


def denominator(y, A_hat, cov_vv):
    A_hat_ = list(A_hat.copy())
    if y in A_hat_:
        A_hat_.remove(int(y))
    return nominator(y, A_hat_, cov_vv)


def nominator(y, A, cov_vv):
    A_ = list(A.copy())
    # A_.append(int(y))
    # s = slice(1, 3)
    sigm_yy = make_slice(cov_vv, [y], [y])

    if 0 == len(A):
        retv = sigm_yy
    else:
        cov_yA = make_slice(cov_vv, [y], A_)
        cov_AA = make_slice(cov_vv, A_, A_)
        cov_Ay = make_slice(cov_vv, A_, [y])
        inv_cov_AA = call_pinv(cov_AA)
        dot_yA_iAA = np.dot(cov_yA, inv_cov_AA)
        dot_yAiAA_Ay = np.dot(dot_yA_iAA, cov_Ay)
        retv = sigm_yy - dot_yAiAA_Ay
    # print('np_nom', retv)
    return retv


def tf_nominator(y, Ain, cov_vv):  # A may comes in
    # A = Ain  # tf.sets.union(Ain, y)
    A = tf.sets.difference(Ain, y)  # make sure that y is not in A

    # sigm_yy = tf.slice(cov_vv, [y.indices[:, 0][0], y.indices[:, 0][0]], [1, 1])
    y_idx = y.indices[:, 0][0]
    sigm_yy = tf.reshape(tf.gather_nd(cov_vv, [ [y_idx, y_idx] ]), [1, 1])

    def if_A_not_empty(y, A, cov_vv, y_idx, sigm_yy):

        cov_yA_row = tf.reshape(tf.slice(cov_vv, [y_idx, 0], [1, cov_vv.shape[1]])[0], [1, -1])
        A_idxs_ = A.indices[:, 0]
        A_idxs = tf.stack([tf.zeros_like(A_idxs_), A_idxs_], axis=1)
        cov_yA = tf.gather_nd(cov_yA_row, [ A_idxs ] )[0]

        A_idxs_d = tf.expand_dims(A_idxs_, axis=1)
        cov_AA_rows = tf.gather_nd(cov_vv, A_idxs_d)
        cov_AA_rowsT = tf.transpose(cov_AA_rows)
        cov_AAT = tf.gather_nd(cov_AA_rowsT, A_idxs_d)
        cov_AA = tf.transpose(cov_AAT)

        # numerical stability of pinv is helped by cov_AA += 1e-5 * eye
        # eye matrix has parameter n.
        cov_AA = tf.linalg.set_diag(cov_AA,
                                    tf.linalg.tensor_diag_part(cov_AA) +
                                    tf.constant(1e-6, dtype=tf.float64))

        cov_Ay_col = tf.slice(cov_vv, [0, tf.reshape(y_idx, [])], [cov_vv.shape[0], 1])[:, 0]
        cov_Ay = tf.gather(cov_Ay_col, [A_idxs_])[0]

        # pinv, Danger of numerical representation loss. When n >> , then det cov_ij -> 0
        pinv = tf.linalg.pinv(cov_AA)
        cov_yAT = tf.transpose(cov_yA)
        mul1 = tf.tensordot(cov_yAT, pinv, [[0], [0]])
        mul2 = tf.tensordot(mul1, cov_Ay, [[0], [0]])
        nom = sigm_yy - mul2

        # nom_pr = tf.print('\ny=', y,
        #                   # '\nAin=', Ain,
        #                   # '\ny_idx=', y_idx,
        #                   # '\nsigm_yy=', sigm_yy,
        #                   #
        #                   # '\ncov_yA_row=', cov_yA_row,
        #                   # '\nA_idxs_=', A_idxs_,
        #                   # '\nA_idxs=', A_idxs,
        #                   # '\ncov_yA=', cov_yA,
        #                   # '\ncov_yAT=', cov_yAT,
        #                   #
        #                   # '\nA_idxs_d=', A_idxs_d,
        #                   # '\ncov_AA_rows=', cov_AA_rows,
        #                   # '\ncov_AA_rowsT=', cov_AA_rowsT,
        #                   # '\ncov_AAT=', cov_AAT,
        #                   # '\ncov_AA=', cov_AA,
        #                   #
        #                   # '\ncov_Ay_col=', cov_Ay_col,
        #                   # '\ncov_Ay=', cov_Ay,
        #                   #
        #                   # '\npinv=', pinv,
        #                   # '\nmul1=', mul1,
        #                   # '\nmul2=', mul2,
        #                   #
        #                   # '\nnom=', nom
        #                   # '\ncov_vv=', cov_vv
        #                   )
        # with tf.control_dependencies([nom_pr]):
        return nom

    nom_ = tf.cond(tf.equal(0, tf.size(A.values)),
                   lambda: sigm_yy,
                   lambda: if_A_not_empty(y, A, cov_vv, y_idx, sigm_yy))

    # nom_pr2 = tf.print(['nom=', nom_],
    #                    output_stream=sys.stdout,
    #                    name='nom_pr2')
    # with tf.control_dependencies([nom_pr2]):
    return nom_


def tf_denominator(y, A_hat, cov_vv):
    A_hat_ = tf.sets.difference(A_hat, y)
    return tf_nominator(y, A_hat_, cov_vv)


# VI. --------------------------------------------------------
def if_delta_cached_is_uptodate(delta_cached, y_st):
    # [y] = tf.reshape(y_st.values[0], [-1])
    y = y_st.values[0]
    delta_cached = tf.reshape(delta_cached, [-1])
    s = tf.reshape(tf.slice(delta_cached, [y], [1]), [])
    false_fn = lambda : False
    true_fn = lambda: True

    bool_uptodate = tf.reshape(tf.case([(tf.equal(s, True), true_fn)], default=false_fn), [])
    return bool_uptodate


# (loop_F) VII. --------------------------------------------------------
def if_delta_cached_is_not_uptodate(y_st, A, A_bar, cov_vv,
                                    delta_cached,
                                    delta_cached_uptodate,
                                    go):
    """
    Update the cache (the else branch here ...)

            if delta_cached_is_uptodate[y_st]:
                break
            else:
                delta_y = 0
       *        nom = nominator(y_st, A, cov_vv)
       *        denom = denominator(y_st, A_bar, cov_vv)
                if np.abs(denom) < 1e-8 or np.abs(nom) < 1e-8:
                    # delta_y = 0
                    pass
                else:
                    delta_y = nom / denom

       *        delta_cached[y_st] = delta_y
       *        delta_cached_is_uptodate[y_st] = True
    """
    # cov_vv = snippets.tf_print2(cov_vv_, [A.values, A_bar.values], 'if_delta_cached_is_not_uptodate: A, A_bar=\n')

    tf_nominator_ = tf.function(tf_nominator)
    tf_denominator_ = tf.function(tf_denominator)

    nom = tf_nominator_(y_st, A, cov_vv)
    denom = tf_denominator_(y_st, A_bar, cov_vv)

    # nom_ = snippets.tf_print2(nom, [nom, denom], 'if_delta_cached_is_not_uptodate: nom, denom=\n')

    denom_is_near_zero = if_denom_is_near_zero(nom, denom)

    delta_y = then_update_delta_y(
        denom_is_near_zero,  # True : don't do anything
        go,                  # False : don't do anything
        nom,
        denom)

    # if (!go) : we have delta_y == 0 and going to update the cache anyway
    # => let delta_y stay!

    # delta_y_pr2 = tf.print(['delta_y=', delta_y, 'y_st.values[0]', y_st.values[0]])
    # with tf.control_dependencies([delta_y_pr2]):
    # pr_op0 = tf.print(['nom=', nom, 'denom=', denom, 'delta_y=', delta_y],
    #                   output_stream=sys.stdout,
    #                   name='print_delta_y')

    with tf.control_dependencies([delta_y]):
        N = delta_cached.shape[0]

    delta_cached_v = tf.Variable(lambda: tf.zeros([N, 1], dtype=tf.float64),
                                 dtype=tf.float64,
                                 shape=[N, 1])
    delta_cached_uptodate_v = tf.Variable(lambda: tf.fill([N, 1], False), shape=[N, 1])

    op01 = tf.assign(delta_cached_v, delta_cached)                    # delta_cached[y_st] = delta_y
    op02 = tf.assign(delta_cached_uptodate_v, delta_cached_uptodate)  # delta_cached_is_uptodate[y_st] = True

    with tf.control_dependencies([cov_vv, delta_y, y_st.values, op01, op02]):
        # => let delta_y stay!
        op11 = tf.case(
            [(go, lambda: tf.scatter_nd_update(op01, [[y_st.values[0], 0]], [delta_y]))],  # delta_cached[y_st] = delta_y
            default=lambda: op01)

        op12 = tf.case(
            [(go, lambda: tf.scatter_nd_update(op02, [[y_st.values[0], 0]], [tf.cast(True, tf.bool)]))],
            default=lambda: op02)

        # [tf.cast([True], tf.float32)])  # delta_cached_is_uptodate[y_st] = True
        # this should be the point when delta_cached gets updated EVEN FOR THE CURRENT SELECTED y_st
        # pr_op = tf.print(['op11=', op11])

    with tf.control_dependencies([op11, op12]):
        # return delta_cached_v, delta_cached_uptodate_v, cov_vv
        return op11, op12


# V.
def while_true_outside(A_A, A_bar_A, V, delta_cached_A, delta_cached_uptodate_A, cov_vv_A):

    cond_D = lambda true, _, A_D, A_bar_D, delta_cached_D, delta_cached_uptodate_D, cov_vv_D: true  # while True:
    def body_D(true, y_st_D, A_D, A_bar_D, delta_cached_D, delta_cached_uptodate_D, cov_vv_D):
        true, y_st_D, A_D, A_bar_D, delta_cached_D, \
        delta_cached_uptodate_D = while_true_inside(
            true, A_D, A_bar_D, V, delta_cached_D, delta_cached_uptodate_D, cov_vv_D)

        # delta_cached_D = snippets.tf_print2(delta_cached_D,
        #                                     [y_st_D.values, A_D.values, A_bar_D.values,
        #                                      delta_cached_D, delta_cached_uptodate_D],
        #                                     'while_true_outside inside body_D : ')

        return [true, y_st_D, A_D, A_bar_D, delta_cached_D, delta_cached_uptodate_D, cov_vv_D]

    N = delta_cached_A.shape[0]
    y_st_ = tf.SparseTensor(indices=[[0, 0]], values=[tf.cast(0, dtype=tf.int64)], dense_shape=(N, 1))

    # delta_cached_A = snippets.tf_print2(delta_cached_A,
    #             [A_A.values, A_bar_A.values,
    #              delta_cached_A, delta_cached_uptodate_A],
    #             'while_true_outside before body_D : ')

    [loop_D, y_st_D_, A_D_, A_bar_D_, delta_cached_D_, delta_cached_uptodate_D_, _] = tf.while_loop(
        cond_D, body_D,
        loop_vars=[True, y_st_, A_A, A_bar_A, delta_cached_A, delta_cached_uptodate_A, cov_vv_A],
        name='whD')

    return y_st_D_, A_D_, A_bar_D_, delta_cached_D_, delta_cached_uptodate_D_


# V.b
def while_true_inside(true, A_D, A_bar_D, V, delta_cached_D, delta_cached_uptodate_D, cov_vv_D):

    N = delta_cached_D.shape[0]

    y_st_ = sparse_argmax_cache_linear(delta_cached_D, A_D, V)
    y_st_D = tf.SparseTensor(indices=[[y_st_, 0]], values=[tf.cast(y_st_, dtype=tf.int64)], dense_shape=(N, 1))

    # VI.
    bool_uptodate = if_delta_cached_is_uptodate(delta_cached_uptodate_D, y_st_D)
    true_ = tf.logical_not(bool_uptodate)

    # true_ = snippets.tf_print2(true_,
    #                 [true, y_st_D.values, A_D.values, A_bar_D.values,
    #                  delta_cached_D, delta_cached_uptodate_D],
    #                 'while_true_inside: ')

    # VII.  then_update_delta_y
    delta_cached_D_, delta_cached_uptodate_D_ = if_delta_cached_is_not_uptodate(
        y_st_D, A_D, A_bar_D, cov_vv_D,
        delta_cached_D,
        delta_cached_uptodate_D,
        go=true_)

    # delta_cached_D_ = snippets.tf_print2(delta_cached_D_,
    #                 [delta_cached_D_, delta_cached_uptodate_D_],
    #                 'while_true_inside, delta_cached_D_: ')

    return true_, y_st_D, A_D, A_bar_D, delta_cached_D_, delta_cached_uptodate_D_


# (body_E) VIII. --------------------------------------------------------
def if_denom_is_near_zero(nom, denom):
    """
    figure out if denom is near zero, therefore
    delta_y shall not be calculated

            if np.abs(denom) < 1e-8 or np.abs(nom) < 1e-8 :
                # delta_y = 0
                pass
            else: ...
    """
    nom = tf.cast(nom, dtype=tf.float64)
    denom = tf.cast(denom, dtype=tf.float64)

    small = tf.constant(1e-7, dtype=tf.float64)

    denom_is_near_zero = tf.logical_or(
        tf.less(tf.abs(denom), small),  # if np.abs(denom) < 1e-8
        tf.less(tf.abs(nom), small)
    )

    # true_if_fn_ = lambda: True
    # else_fn_ = lambda: False

    # denom_is_near_zero = tf.case([(
    #     tf.reshape(
    #         tf.logical_or(
    #             tf.less(tf.abs(denom), small),  # if np.abs(denom) < 1e-100
    #             tf.equal(nom,                   # or nom == 0:
    #                      tf.cast(0., dtype=tf.float64))
    #             ),
    #     []), true_if_fn_)], default=else_fn_)

    return denom_is_near_zero  # True / False


# IX. --------------------------------------------------------
def assign_delta_y(nom_, denom_ ):
    delta_y_ = tf.cast(tf.divide(tf.reshape(nom_, []), tf.reshape(denom_, [])), dtype=tf.float64)
    # delta_y = snippets.tf_print2(delta_y_, [delta_y_], 'tf.print delta_y_F: ')

    return delta_y_


# (loop_F) IX. --------------------------------------------------------
def then_update_delta_y(denom_is_near_zero, true_, nom, denom):
    """
    if denom is not zero, update delta_y

            delta_cached[y_st] = delta_y
            delta_cached_ is_uptodate[y_st] = True

    """
    delta_y = tf.Variable(0., dtype=tf.float64, name="delta_y")  # delta_y = 0

    cond_F = lambda denom_is_near_zero, true__, _, nom_, denom_: \
        tf.logical_and(
            tf.logical_not(denom_is_near_zero), true__)

    def body_F(denom_is_near_zero, true__, _, nom_, denom_):

        # delta_y_F_ = tf.cast(tf.divide(tf.reshape(nom_, []), tf.reshape(denom_, [])),dtype=tf.float64)
        # delta_y_F = snippets.tf_print2(delta_y_F_, [delta_y_F_], 'tf.print delta_y_F: ')
        delta_y_F = tf.function(assign_delta_y)(nom_, denom_)
        run_only_once = tf.constant(False)

        return [False, run_only_once, delta_y_F, nom_, denom_]  # is this ok ?

    [cd_F, true_F, delta_y, nom_F, denom_F] = tf.while_loop(
        cond_F, body_F,
        loop_vars=[tf.reshape(denom_is_near_zero, []), true_, delta_y, nom, denom],
        name='while_in_then_update_delta_y')

    return delta_y


# X. --------------------------------------------------------
def append_to_A_remove_from_A_bar(A, A_bar, y_st):

    A_ = tf.sets.union(A, y_st)
    A_bar_ = tf.sets.difference(A_bar, y_st)

    return A_, A_bar_


#===============================================================
# Placement algorithm 2
#===============================================================
def sparse_placement_algorithm_2(cov_vv, k, COVER_spatial):
    # V.
    '''
    assume U is empty
    assume V = S
    A - selection of placements eventually lngth = k
    y = len(V) + len(S) -> MOVE selections from V to S
    '''
    # I.
    # A = [] # selected_indexes
    with tf.name_scope("tf-placement-2"):
        INF = tf.constant(1e8, dtype=tf.float64)
        N = cov_vv.shape[0]
        assert_op = tf.Assert(tf.equal(N, COVER_spatial[0]*COVER_spatial[1]*COVER_spatial[2]), [N])  # 625
        with tf.control_dependencies([assert_op]):
            A = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))
        # V = np.linspace(0, cov_vv.shape[0]-1, cov_vv.shape[0], dtype=np.int64)
        values01 = tf.cast(tf.linspace(0., tf.cast(cov_vv.shape[0]-1, tf.float32), cov_vv.shape[0]), tf.int64)  # values
        values02 = tf.cast(tf.linspace(0., 0., cov_vv.shape[0]), tf.int64)  # values
        indxes = tf.stack([values01, values02], axis=1)
        V = tf.SparseTensor(indices=indxes, values=values01, dense_shape=[N, 1])

        # calc all delta_cached_iters
        delta_cached_iters = tf.Variable(tf.zeros([N, k], dtype=tf.float64), dtype=tf.float64, shape=[N, k])

        # II.
        A_bar = V  # complementer set to A.

        # delta_cached = [[INF, INF, ..]]
        delta_cached = tf.Variable(tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)),
                                   dtype=tf.float64,
                                   shape=[N, 1])
        A_selection_and_delta = tf.Variable(tf.zeros([k, 2], dtype=tf.float64),
                                            dtype=tf.float64,
                                            shape=[k, 2])
        # ===========================================================================
        # while len(A) < k:
        cond_A = lambda len_A, A_A, A_bar_A, delta_cached_A, cov_vv_A, dci, A_sel_n_delta: tf.less(len_A, k)  # dci is delta_cached_iters
        def body_A(len_A, A_A, A_bar_A, delta_cached_A, cov_vv_A, dci, A_sel_n_delta):

            # IV.  set_all_delta_cach_to_false
            # in each round we make decision on updated value
            # delta_cached_is_uptodate = [[False, False, ..]]
            delta_cached_uptodate_A = delta_cached_uptodate_empty(N)

            # delta_cached_uptodate = tf.Variable(tf.fill([N, 1], cache_vals), shape=[N, 1])
            # op2 = tf.scatter_nd_update(delta_cached_uptodate, [[y_st.values[0], 0]],
            #                            [cache_vals])  # deltdelta_cached_uptodate[y_st] = True
            # op = tf.s
            # delta_cached_uptodate_A = tf.Variable(tf.fill([N, 1], False), shape=[N, 1])

            # V.
            # while True:
            y_st_A, A_A, A_bar_A, delta_cached_A, delta_cached_uptodate_A \
                = while_true_outside(
                    A_A, A_bar_A, V,
                    delta_cached_A,
                    delta_cached_uptodate_A,
                    cov_vv_A)

            # pr_yst_op = tf.print(['y_st_A=', y_st_A.values[0], 'A_A=', A_A.values, A_A.indices,
            #                       '\ndelta_cached_A: \n', delta_cached_A])
            # with tf.control_dependencies([pr_yst_op]):
            #     pr_yst_op2 = tf.print(['before: ', 'A_A=', A_A.values, 'A_bar_A=', A_bar_A.values])

            # X.
            A_A, A_bar_A = append_to_A_remove_from_A_bar(A_A, A_bar_A, y_st_A)

            #     pr_yst_op3 = tf.print(['after: ', 'A_A=', A_A.values, 'A_bar_A=', A_bar_A.values,
            #                            # '\ndci: \n', dci,
            #                            '\ndelta_cached_iters: \n', delta_cached_iters])
            # with tf.control_dependencies([pr_yst_op2, pr_yst_op3]):

            len_A = tf.size(A_A.values)

            # t_i_ = tracers_loc_i - tf.fill(dims=tracers_loc_i.shape, value=tr_mean)
            indx_stack = tf.cast(tf.expand_dims(tf.linspace(0., tf.cast(N-1, dtype=tf.float32), N), axis=1), dtype=tf.int32)
            len_A_fill = tf.fill(dims=indx_stack.shape, value=len_A-1)
            indx_stack_ = tf.reshape(tf.stack([indx_stack, len_A_fill], axis=1), [-1, 2])

            # -------------------------------------------------
            # 1.
            A_sel_and_delta_var = tf.Variable(lambda: tf.zeros([k, 2], dtype=tf.float64),
                                              dtype=tf.float64, shape=[k, 2])
            # 2.
            A_sel_and_delta_before = tf.assign(A_sel_and_delta_var, A_sel_n_delta)

            # 3.
            A_sel_and_delta_after_ = tf.scatter_nd_update(A_sel_and_delta_before, [[len_A-1, 0]], [tf.cast(y_st_A.values[0], dtype=tf.float64)])  # update y_st
            A_sel_and_delta_after = tf.scatter_nd_update(A_sel_and_delta_after_, [[len_A-1, 1]], delta_cached_A[y_st_A.values[0]])  # update delta

            # -------------------------------------------------
            # 1. create a new Var and fill with its previous value, then add the current update
            dci_var = tf.Variable(lambda: tf.zeros([N, k], dtype=tf.float64), dtype=tf.float64, shape=[N, k])

            # 2. ... fill with its previous value, then add the current update
            dci_set_before = tf.assign(dci_var, dci)

            # 3. ... add the current update
            dci_set_after = tf.scatter_nd_update(dci_set_before, indx_stack_, tf.reshape(delta_cached_A, [-1]))

            pr_op = tf.print(['y_st=', y_st_A.values[0], 'delta=', delta_cached_A[y_st_A.values[0], 0]])

            # -------------------------------------------------
            # 1. delta_cached update interleaved with A_ordered_set update
            delta_cached_A_cpy = tf.Variable(lambda: tf.zeros([N, 1], dtype=tf.float64),
                                             dtype=tf.float64,
                                             shape=[N, 1])
            # 2.
            delta_cached_A_z = tf.assign(delta_cached_A_cpy, delta_cached_A)

            # 3.
            with tf.control_dependencies([dci_set_after
                                          # , pr_op
                                          ]):
                # we have saved delta_cached_A, but the value for y_st_A will never change again
                # We may zero it now - after delta_cache is separate from A selections.
                op_z = tf.scatter_nd_update(delta_cached_A_z, [[y_st_A.values[0], 0]], [0.])  # delta_cached[y_st] = 0
                pr_op_z = tf.print(['y_st=', y_st_A.values[0], 'delta_z=', delta_cached_A_z[y_st_A.values[0], 0]])

            with tf.control_dependencies([op_z, pr_op_z]):
                len_A_ret = len_A

            return [len_A_ret, A_A, A_bar_A, op_z, cov_vv_A, dci_set_after, A_sel_and_delta_after]

        # delta_cached = snippets.tf_print2(delta_cached, [tf.size(A.values), A.values, A_bar.values, delta_cached], 'starting with: ')
        # delta_cached_iters -> delta_cached_iters
        # A_selection_and_delta -> A_selection_and_delta_
        [loop_A, A, A_bar, delta_cached, _, delta_cached_iters_, A_selection_and_delta_] = tf.while_loop(
            cond_A, body_A,
            loop_vars=[tf.size(A.values), A, A_bar, delta_cached, cov_vv, delta_cached_iters, A_selection_and_delta],
            name='whA')

        # A_A is modelling a SET, thus working like a SET: does not care about the order
        #  => We loose the order of the selection in A_A
        # pr_yst_op = tf.print(['A=', A.values, A.indices, '\ndelta_cached_iters=', delta_cached_iters])
        with tf.control_dependencies([loop_A, A.values, delta_cached, delta_cached_iters_, A_selection_and_delta_]):

            # delta_cached_iters_tensor = tf.SparseTensor(indices=?, values=delta_cached_iters, dense_shape=[N, 1])
            return A, loop_A, delta_cached_iters_, A_selection_and_delta_  # A shall contain k elements.


def argmax_(A, A_bar, V, cov_vv):
    y_st = -1  # index of coordinate
    delta_st = -1
    for y in V:  # improved in algorithm 2
        if y in A:  # if y in A then dont consider: continue
            continue  # skip delta
        else:  # y is not in A, consider, calc delta.
            nom = nominator(y, A, cov_vv)
            denom = denominator(y, A_bar, cov_vv)
            # print('nom=', nom, 'denom=', denom)
            if np.abs(denom) < 1e-8 or np.abs(nom) < 1e-8:
                delta_y = 0
            else:
                delta_y = nom / denom

        if delta_st < delta_y:
            delta_st = delta_y  # update largest delta yet, delta_star
            y_st = y  # update index
    return y_st, delta_st


def placement_algorithm_1(cov_vv, k):
    '''
    assume U is empty
    assume V = S, indexes where there may be sensors [0 .. cov_vv.shape[0])
    A - selection of placements eventually lngth = k
    y = len(V) + len(S) -> MOVE selections from V to S
    '''
    A = []  # selected_indexes
    V = np.linspace(0, cov_vv.shape[0]-1, cov_vv.shape[0], dtype=np.int64)
    A_bar = []  # complementer set to A.
    for i in V:  # iterate indices where we can have sensors
        A_bar.append(i)

    while len(A) < k:  # "for j=1 to k"
        y_st, _ = argmax_(A, A_bar, V, cov_vv)  # choose y* mutual information which is the highest of the nom/denom in the formula given
        A.append(y_st)  # add largest MI's corresponding index
        A_bar.remove(y_st)  # remove it from complementer set
    return A  # A contains k elements.


#=========================================================
# Algorithm 2
#=========================================================
def placement_algorithm_2(cov_vv, k):
    '''
    assume U is empty
    assume V = S
    A - selection of placements eventually lngth = k
    y = len(V) + len(S) -> MOVE selections from V to S
    '''
    # I.
    A = [] # selected_indexes
    V = np.linspace(0, cov_vv.shape[0]-1, cov_vv.shape[0], dtype=np.int64)
    A_bar = [] # complementer set to A.
    delta_cached = []
    delta_cached_is_uptodate = []
    INF = 1e1000

    # II.
    for i in V: # iterate indices where we can have sensors
        A_bar.append(i)
        delta_cached.append(INF)
        delta_cached_is_uptodate.append(0)

    # III.
    while len(A) < k: # "for j=1 to k"
        y_st = -1  # index of coordinate
        delta_st = -1
        # "S \ A do current_y <- false"

        # IV.  set_all_delta_cach_to_false
        for i in range(len(delta_cached_is_uptodate)):  # in each round we make decision on updated value
            delta_cached_is_uptodate[i] = False

        # V.
        while True:
            y_st = argmax_cache_linear(delta_cached, A, V)  # re-implement with priority-queue

            # VI.
            if delta_cached_is_uptodate[y_st]:
                print('y*=', y_st)
                break

            # VII.  then_update_delta_y
            else:
                delta_y = 0
                nom = nominator(y_st, A, cov_vv)
                denom = denominator(y_st, A_bar, cov_vv)

                # VIII.  if_denom_is_near_zero + if_denom_is_near_zero_check
                if np.abs(denom) < 1e-8 or np.abs(nom) < 1e-8:  # here: delta_y = 0
                    pass

                # IX.
                else:
                    delta_y = nom / denom

                print('delta_y=', delta_y, 'y_st=', y_st)

                delta_cached[y_st] = delta_y
                delta_cached_is_uptodate[y_st] = True

        # X.
        A.append(y_st)  # add largest MI's corresponding index .. found so far,
        # .. which is the first updated value in this round exceeding
        # .. all the rest of not yet updated previous round values (that is "good enough")
        A_bar.remove(y_st)  # remove it from complementer set

        # XI.
        # plot3D(delta_cached, 12, 12)
        # print("plot, current A is:", A)
    return A  # A contains k elements.


# ==============================================================
# Placement algorithm 3
# ==============================================================
# inputs: cov_vv, k, split, cutoff (scaled to represent spatial distance, but algorithm 3 knows only index distance)
# outputs: delta_cached_iters(plot each delta_cache per k), A(selected_points)
# local kernels (cov_vv)
# new cov that models exponential decay in dependecy with distance.
def sparse_placement_algorithm_3(cov_vv, k, COVER_spatial, cutoff):

    [I0, I1, I2] = COVER_spatial
    ts0 = tf.timestamp()

    # I.
    INF = tf.constant(1e8, dtype=tf.float64)
    N = cov_vv.shape[0]
    assert_op = tf.Assert(tf.equal(N, COVER_spatial[0]*COVER_spatial[1]*COVER_spatial[2]), [N])
    with tf.control_dependencies([assert_op]):
        A = tf.SparseTensor(indices=np.empty((0, 2), dtype=np.int64), values=np.empty((0), dtype=np.int64), dense_shape=(N, 1))

    # II.2 create A_bar
    values01 = tf.cast(tf.linspace(0., tf.cast(cov_vv.shape[0] - 1, tf.float32), cov_vv.shape[0]), tf.int64)  # values
    values02 = tf.cast(tf.linspace(0., 0., cov_vv.shape[0]), tf.int64)  # values
    indxes = tf.stack([values01, values02], axis=1)
    V = tf.SparseTensor(indices=indxes, values=values01, dense_shape=[N, 1])
    A_bar = V  # complementer set to A.
    len_A = tf.size(A.values)

    # II. foreach y in S do: delta_y <- H(y) - Hhat_epsilon(y | V\y)
    # alg2 part 3. == alg3 part 1. the update of all delta_y only happens once here,
    #   taken out from within the loop j=1 to k in alg2
    delta_cached = tf.Variable(tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)), dtype=tf.float64, shape=[N, 1])

    #
    # the arrangement of body_A is such, that we come in with an up-to-date delta_cache,
    # at the beginning we do the selection for index 0
    # and at the end we save the updated delta_cache (into delta_cached_iters) at index 1,
    # because 0 is already up-to-date
    #
    delta_cached_iters = tf.Variable(tf.zeros([N, k], dtype=tf.float64), dtype=tf.float64, shape=[N, k])

    # Update for 1 k all the the delta_cache
    cond_D = lambda y_D, A_D, A_bar_D, delta_cached : tf.less(y_D, N)
    def body_D(y_D, A_D, A_bar_D, delta_cached_D):

        pr_iD = tf.print(['y_D =', y_D, tf.timestamp() - ts0])

        delta_cached_assign = tf.Variable(lambda : tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)), dtype=tf.float64, shape=[N, 1])
        dci_set_before = tf.assign(delta_cached_assign, delta_cached_D)

        tf_nominator_ = tf.function(tf_nominator)
        tf_denominator_ = tf.function(tf_denominator)

        y = tf.SparseTensor(indices=tf.cast([[y_D, 0]], dtype=np.int64),
                            values=tf.cast([y_D], dtype=np.int64),
                            dense_shape=[N, 1])

        nom = tf_nominator_(y, A_D, cov_vv)
        denom = tf_denominator_(y, A_bar_D, cov_vv)

        # nom_ = snippets.tf_print2(nom, [nom, denom], 'if_delta_cached_is_not_uptodate: nom, denom=\n')

        denom_is_near_zero = if_denom_is_near_zero(nom, denom)

        delta_y = then_update_delta_y(
            denom_is_near_zero,  # True : don't do anything
            True,  # False : don't do anything
            nom,
            denom)

        update = tf.reshape(delta_y, [], name="update")
        indexes = tf.cast([y_D, 0], dtype=tf.int32, name='indexes')
        dci_set_afterD = tf.scatter_nd_update(dci_set_before, [indexes], [update])

        pr_op0 = tf.print(['y_D=', y_D, 'indexes=', indexes, 'update=', update])
        with tf.control_dependencies([pr_iD, dci_set_afterD
                                      # , pr_op0
                                      ]):
            y_D += 1

        return y_D, A_D, A_bar_D, dci_set_afterD

    # loop_D is last value of y, when cond was still called, but while loop is finished
    [loop_D, _, _, delta_cached0] \
        = tf.while_loop(cond_D, body_D,
                        loop_vars=[0, A, A_bar, delta_cached], name='whD')

    # now the delta cache is up to date (first pass)
    # save it to delta_cache_iters

    indx_stack = tf.cast(tf.expand_dims(tf.linspace(0., tf.cast(N - 1, dtype=tf.float32), N), axis=1), dtype=tf.int32)
    len_A_fill = tf.fill(dims=indx_stack.shape, value=0)  # which column to fill
    indx_stack_ = tf.reshape(tf.stack([indx_stack, len_A_fill], axis=1), [-1, 2])
    dci_set_after = tf.scatter_nd_update(delta_cached_iters, [indx_stack_], [tf.reshape(delta_cached0, [-1])])
    print_op = tf.print(['dci_set_after=', dci_set_after], output_stream=sys.stdout, name='print_dci')

    # now the first column of delta_cache_iters is up-to-date
    # return indx_stack_, print_op

    # III. for j=1 to k do ===========================================================================
    cond_A = lambda i, A_A, A_bar_A, delta_cached_A, dci: tf.less(i, k-1)  # while len(A) < k: #
    def body_A(i, A_A, A_bar_A, delta_cached_A, dci):

        # pr_op2 = tf.print(['A_A.values=', A_A.values, 'V.values=', V.values], output_stream=sys.stdout, name='print_dci')
        # with tf.control_dependencies([pr_op2]):

        # IV. y* <- argmax_y delta_y
        y_st_ = sparse_argmax_cache_linear(delta_cached_A, A_A, V)
        pr_sel = tf.print(['y_st=', y_st_, 'delta=', delta_cached_A[y_st_]])
        y_st_A = tf.SparseTensor(indices=[[y_st_, 0]], values=[tf.cast(y_st_, dtype=tf.int64)], dense_shape=(N, 1))

        # V. A <- A U y*
        intersect_size_A0 = tf.reduce_sum(tf.sets.size(tf.sets.intersection(A_A, y_st_A)))
        itaA0 = tf.Assert(tf.equal(0, intersect_size_A0), [intersect_size_A0, y_st_A.values, A_A.values])

        A_A, A_bar_A = append_to_A_remove_from_A_bar(A_A, A_bar_A, y_st_A)

        # check that y_st not in A
        intersect_size_A = tf.reduce_sum(tf.sets.size(tf.sets.intersection(A_A, y_st_A)))
        itaA = tf.Assert(tf.equal(1, intersect_size_A), [intersect_size_A, y_st_A.values, A_A.values])

        #
        # Consider that zeroing of the selected point (which we would not touch otherwise),
        # .. like in algo_2, for visualization. (The delta_cached_A is already in dci,
        # so this value will just not poison the next round) => delta_cached_A[y_st_] = 0
        #
        delta_cached_Av = tf.Variable(lambda: tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)),
                                      dtype=tf.float64,
                                      shape=[N, 1])  # create a new var
        delta_cached_Avs = tf.assign(delta_cached_Av, delta_cached_A)  # copy the value over
        update = tf.reshape(0., [], name="update")
        indexes = tf.cast([y_st_, 0], dtype=tf.int32, name='indexes')
        delta_cached_A_clear = tf.scatter_nd_update(delta_cached_Avs, [indexes], [update])  # update & carry the new val
        # check
        pr_cl = tf.print(['i=', i,
                          'delta_cached_A_clear[y_st_] != 0. :', y_st_,
                          delta_cached_A_clear[y_st_, 0]],
                         output_stream=sys.stdout,
                         name='print_dci')
        a_cl = tf.Assert(tf.equal(delta_cached_A_clear[y_st_, 0], 0.),
                         [i,
                          'delta_cached_A_clear[y_st_] != 0. :',
                          y_st_,
                          delta_cached_A_clear[y_st_, 0]])

        # foreach y NEAR (y_st, cutoff) delta_cache[y] = ...
        # VI. foreach y E N(y*; epsilon) do:
        # update cache, only the delta_ys should be updated,
        # where the cov_vv > 0 <=> ones near y* in epsilon distance
        # -> THIS is why complexity of function is reduced from O(N*N3) to O(epsilon*N2)

        # with tf.control_dependencies([pr_cl, a_cl]):
        strde0 = I2 * I1
        strde1 = I2
        strde2 = 1
        i0 = y_st_ // strde0
        i1 = (y_st_ - i0*strde0) // strde1
        i2 = (y_st_ - i0*strde0 - i1*strde1) // strde2
        a0 = tf.Assert(tf.less(i0, I0), ['i=', y_st_, 'i0, I0=', i0, I0, 'i1, I1=', i1, I1, 'i2, I2=', i2, I2])
        a1 = tf.Assert(tf.less(i1, I1), ['i=', y_st_, 'i0, I0=', i0, I0, 'i1, I1=', i1, I1, 'i2, I2=', i2, I2])
        a2 = tf.Assert(tf.less(i2, I2), ['i=', y_st_, 'i0, I0=', i0, I0, 'i1, I1=', i1, I1, 'i2, I2=', i2, I2])
        with tf.control_dependencies([a0, a1, a2,
                                      # pr_cl,
                                      pr_sel,
                                      a_cl, itaA0, itaA]):
            i0_ = i0

        cond0 = lambda j0, J0, A0, Abar0, y0, delta_cached0: tf.less(j0, J0)

        def body0(j0, J0, A0, Abar0, y0, delta_cached0):
            pr_i = tf.print(['j0 =', j0, tf.timestamp() - ts0])

            cond1 = lambda j1, J1, A1, Abar1, y1, delta_cached1: tf.less(j1, J1)
            def body1(j1, J1, A1, Abar1, y1, delta_cached1):

                cond2 = lambda j2, J2, A2, Abar2, y2, delta_cached2: tf.less(j2, J2)
                def body2(j2, J2, A2, Abar2, y2, delta_cached2):

                    #
                    # Here we have j0, j1, j2 -> yj
                    #   which y is to be updated in delta_cached_A;
                    #

                    yj = strde0*j0 + strde1*j1 + strde2*j2
                    yjA = tf.SparseTensor(indices=[[yj, 0]],
                                          values=[tf.cast(yj, dtype=tf.int64)],
                                          dense_shape=(N, 1))

                    delta_cached2v = tf.Variable(lambda: tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)),
                                                 dtype=tf.float64,
                                                 shape=[N, 1])  # create a new var
                    delta_cached2vs_ = tf.assign(delta_cached2v, delta_cached2)  # copy the value over

                    # repeat the clearing step (have done outside) TODO: no effect
                    update2 = tf.reshape(0., [], name="update")
                    indexes2 = tf.cast([yj, 0], dtype=tf.int32, name='indexes')
                    delta_cached2vs = tf.scatter_nd_update(delta_cached2vs_, [indexes2],
                                                           [update2])  # update & carry the new val

                    #
                    # switching delta_cached2vs to hold just the original and
                    #  update the delta_cached2 -> delta_cached2o_ works, but not the other way around:
                    # : it does not write the values in A over
                    #

                    A2m = tf.sets.difference(A2, yjA)
                    Abar2m = tf.sets.difference(Abar2, yjA)
                    _, _, _, delta_cached2o_ = body_D(yj, A2m, Abar2m, delta_cached2)

                    # avoid updating the cache (in the point around which we are updating: not enough)
                    #                          -> any of the points in A
                    intersect_size_y_st = tf.reduce_sum(tf.sets.size(tf.sets.intersection(A2, y2)))
                    ita = tf.Assert(tf.equal(1, intersect_size_y_st), [intersect_size_y_st, y2.values, A2.values])

                    intersect_size_yj = tf.reduce_sum(tf.sets.size(tf.sets.intersection(A2, yjA)))
                    delta_cached2o = tf.case([(tf.less(0, intersect_size_yj),       # yj in A2
                                               lambda: delta_cached2vs)],        # avoid
                                             default=lambda: delta_cached2o_)    # update

                    pr_j2 = tf.print([  #'j0, j1, j2, y_st, yj, its, dci, dct=',
                        j0, j1, j2,
                        y2.values[0], yj, intersect_size_yj,
                        delta_cached2o[yj, 0],
                        delta_cached2o_[yj, 0]])

                    with tf.control_dependencies([
                        delta_cached2vs,
                        pr_j2,  # TODO: if I print here, the cache update seems to work, otherwise NOT!
                        ita,
                        delta_cached2o]):
                        j2_ = j2+1
                    return j2_, J2, A2, Abar2, y2, delta_cached2o

                [loop2, _, _, _, _, delta_cached1o] = tf.while_loop(
                    cond2, body2,
                    loop_vars=[tf.reduce_max([i2 - cutoff, 0]),
                               tf.reduce_min([i2 + cutoff, I2]),
                               A1, Abar1, y1, delta_cached1],
                    name='wh1')

                # pr_j1 = tf.print(['pr1: j1, delta_cached1o[y_st_, 0]=', j1, delta_cached1o[y_st_, 0]])
                with tf.control_dependencies([loop2  #, pr_j1
                                              ]):
                    j1_ = j1+1
                return j1_, J1, A1, Abar1, y1, delta_cached1o

            [loop1, _, _, _, _, delta_cached0o] = tf.while_loop(
                cond1, body1,
                loop_vars=[tf.reduce_max([i1 - cutoff, 0]),
                           tf.reduce_min([i1 + cutoff, I1]),
                           A0, Abar0, y0, delta_cached0],
                name='wh1')

            # pr_j0 = tf.print(['pr0: j0, delta_cached0o[y_st_, 0]=', j0, delta_cached0o[y_st_, 0]])
            with tf.control_dependencies([pr_i, loop1  #, pr_j0
                                          ]):
                j0_ = j0+1
            return j0_, J0, A0, Abar0, y0, delta_cached0o

        with tf.control_dependencies([a0, a1, a2,
                                      # pr_cl,
                                      a_cl]):
            [loop0, _, _, _, _, delta_cached_Ao] = tf.while_loop(
                cond0, body0,
                loop_vars=[tf.reduce_max([i0_-cutoff, 0]),
                           tf.reduce_min([i0_+cutoff, I0]),
                           A_A, A_bar_A, y_st_A, delta_cached_A_clear],
                name='wh0')

        # At this point we have updated delta_cached_Ao, so we are about to select the next y_st
        # Before doing that, we should update delta cached iters (the next selection will be based on that)
        # .. and consider that zeroing of the selected point (which we would not touch otherwise),
        # indx_stackA = tf.cast(tf.expand_dims(tf.linspace(0., tf.cast(N - 1, dtype=tf.float32), N), axis=1),
        #                       dtype=tf.int32)
        with tf.control_dependencies([delta_cached_Ao]):

            # it seems it is hard to stop the incoming delta_cached to have a value at y_st_
            # TODO: investigate
            # try again ... delta_cached_Ao[y_st_] = 0
            delta_cached_iv = tf.Variable(lambda: tf.multiply(INF, tf.ones([N, 1], dtype=tf.float64)),
                                          dtype=tf.float64,
                                          shape=[N, 1])  # create a new var
            delta_cached_ivs = tf.assign(delta_cached_iv, delta_cached_Ao)  # copy the value over
            updatei = tf.reshape(0., [], name="update")
            indexesi = tf.cast([y_st_, 0], dtype=tf.int32, name='indexes')
            delta_cached_i_clear = tf.scatter_nd_update(delta_cached_ivs, [indexesi],
                                                        [updatei])  # update & carry the new val
            # ... delta_cached_Ao[y_st_] = 0

            len_A_fillA = tf.fill(dims=indx_stack.shape, value=i+1)  # which column to fill
            indx_stackA_ = tf.reshape(tf.stack([indx_stack, len_A_fillA], axis=1), [-1, 2])
            dci_set_afterA = tf.scatter_nd_update(dci,
                                                  [indx_stackA_],
                                                  [tf.reshape(delta_cached_i_clear, [-1])])

            # make sure we have 0 in dci_set_afterA[y_st_, i+1]
            print_opi = tf.print(['i=', i,
                                  'delta_cached_Ao[y_st_]=', y_st_,
                                  delta_cached_Ao[y_st_, tf.cast(0, tf.int64)]],
                                 output_stream=sys.stdout,
                                 name='print_dci')
            print_opo = tf.print(['i=', i,
                                  'dci_set_afterA[y_st_, i+1]=', y_st_,
                                  dci_set_afterA[y_st_, tf.cast(i+1, tf.int64)]],
                                 output_stream=sys.stdout,
                                 name='print_dci')

        with tf.control_dependencies([loop0,
                                      # print_opi, print_opo
                                      ]):
            i_ = i+1
        return i_, A_A, A_bar_A, delta_cached_Ao, dci_set_afterA

    [loop_A, A, A_bar, delta_cachedr, delta_cached_iters_] = tf.while_loop(
        cond_A, body_A,
        loop_vars=[0, A, A_bar, delta_cached0, dci_set_after],
        name='whA')

    #
    # here we have the last cache, up-to-date, but we have not selected y_st (from that last one) yet.
    #
    y_st_l = sparse_argmax_cache_linear(delta_cachedr, A, V)
    y_st_last = tf.SparseTensor(indices=[[y_st_l, 0]], values=[tf.cast(y_st_l, dtype=tf.int64)], dense_shape=(N, 1))
    A, A_bar = append_to_A_remove_from_A_bar(A, A_bar, y_st_last)

    return A, delta_cachedr, delta_cached_iters_

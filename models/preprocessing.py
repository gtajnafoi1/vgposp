import os
import pandas as pd
import numpy as np
import tensorflow as tf
import yaml
import utils.data_generation as ut
from typing import List

dtype = np.float64


class NormalizingParams:
    def __init__(self):
        # Normalization parameters: supposed to be used everywhere, Computed from vtk training data
        self.COLUMNS_MEAN = np.array([225.11911961, 66.54242329, 7.32220308, 17.6831950, 3.2987280])
        self.COLUMNS_STDEV = np.array([2.17425109, 1.71791751, 0.96232142, 2.2824182, 1.5928748])
        self.tracer_MEAN = np.array([0.0018159087825037148])
        self.tracer_STDEV = np.array([0.0007434639347162126])

    def get_normalizing_parameters(self):
        # Normalization parameters: supposed to be used everywhere
        # Computed from vtk training data
        return self.COLUMNS_MEAN, self.COLUMNS_STDEV, self.tracer_MEAN, self.tracer_STDEV


class FromYAML:
    def __init__(self, config_path):
        self.config_path = config_path

    def load_config(self):
        if len(self.config_path) > 0:
            with open(self.config_path) as f:
                return yaml.safe_load(f)


class ModelParameters:
    def __init__(self, config_path):
        self.model_config = FromYAML(config_path).load_config()
        self.batch_size = self.get('batch_size')  # segment training data into batch_sizes
        self.num_iters = self.get('num_iters')  # 50  #4  500
        self.num_logs = self.get('num_logs')
        self.num_samples = self.get('num_samples')
        self.observation_noise_variance = self.get('observation_noise_variance')  # 1e-3
        self.learning_rate = self.get('learning_rate')
        self.predictive_noise_variance = self.get('predictive_noise_variance')
        self.jitter = self.get('jitter')
        self.model_saver_path = self.get('model_saver_path')  # './vgp/saved_variables_dataset_1_gp'
        self.model_load_path = self.get('model_load_path')

    def get(self, key: str):
        return self.model_config.get(key, None)


class TrainingData:
    def __init__(self, config_path):
        super().__init__()
        self.config_path = config_path
        self.data_config = FromYAML(config_path).load_config()
        self.all_observations = None
        self.all_index_points = None

        # FILTERS: select subset of all_indices and all_observations
        self.num_training_points_ = self.get('num_training_points_')
        self.num_inducing_points_ = self.get('num_inducing_points_')
        self.num_predictive_index_points_ = self.get('num_predictive_index_points_')
        self.num_predictive_index_points_one_dir = self.get('num_predictive_index_points_one_dir')
        self.num_test_points_ = self.get('num_test_points_')
        self.generated_observation_noise_variance = self.get('generated_observation_noise_variance')
        self.num_feature_dims = self.get('num_feature_dims')
        self.coordinate_range = self.get('coordinate_range')
        self.data_location = self.get('data_location')
        self.path_to_data = self.get('path_to_data')

        self.obs_column_name = self.get('obs_column_name')
        self.obs_index_points_column_names = self.get('obs_index_points_column_names')
        self.obs = None
        self.obs_index_points = None
        self.inducing_index_points = None
        self.predictive_index_points = None

        self.columns_mean = self.get('columns_mean') # [225.11911961, 66.54242329, 7.32220308, 17.6831950, 3.2987280]
        self.columns_stdev = self.get('columns_stdev') # [2.17425109, 1.71791751, 0.96232142, 2.2824182, 1.5928748]
        self.tracer_mean = self.get('tracer_mean') # [0.0018159087825037148]
        self.tracer_stdev = self.get('tracer_stdev') #  [0.0007434639347162126]

    def generate_training_data(self, noise):
        raise NotImplemented

    def get(self, key: str):
        return self.data_config.get(key, None)

    def load_training_data(self, training_data_path: str):
        self.num_feature_dims = self.get('num_feature_dims')  # TODO this should be calc from num of useful columns
        self.feature_dims = self.get('feature_dims')

        self.predictive_index_points = generate_5d_idx(self.num_predictive_index_points_, coord_range=self.coordinate_range)
        self.inducing_index_points = generate_5d_idx(self.num_inducing_points_, coord_range=self.coordinate_range)

        df = pd.read_csv(training_data_path, encoding='utf-8', engine='c')
        all_obs_index_points = df.loc[:, self.obs_index_points_column_names].to_numpy()[np.newaxis, ...] #.squeeze(0)
        all_obs = df.loc[:, self.obs_column_name].to_numpy()[np.newaxis, ...] #.squeeze(0)

        self.obs_index_points, self.test_obs_index_points = self.load_randomize_select_train_test(all_obs_index_points)
        self.obs, self.test_obs = self.load_randomize_select_train_test(all_obs)

    def load_randomize_select_train_test(self, xyzt_idx):
        ''' Load CSV data, randomize and divide to train and test sets
        '''
        SELECT_ROW_NUM = xyzt_idx.shape[1] // 2
        np.random.shuffle(xyzt_idx)
        if len(xyzt_idx.shape) > 2:
            train_dataset = xyzt_idx[:, :SELECT_ROW_NUM, :]
            test_dataset = xyzt_idx[:, SELECT_ROW_NUM:, :]
        else:
            train_dataset = xyzt_idx[:, :SELECT_ROW_NUM]
            test_dataset = xyzt_idx[:, SELECT_ROW_NUM:]
        return train_dataset, test_dataset

        # -------------------------------------------
        # print('INFO: [TrainingData.load_training_data] using filters on training data.')
        # SIZEOFDATASET = 200
        #
        # input_x_axis = df['Points:0']
        # input_y_axis = df['Points:1']
        # input_z_axis = df['Points:2']
        # input_pressure = df['Pressure']
        # input_temperature = df['Temperature']
        # target_tracer = df['Tracer']
        #
        # ds_idx_length = df.shape[0]
        # ds_idx_arr = np.linspace(0, ds_idx_length -1, ds_idx_length ).T
        # np.random.shuffle(ds_idx_arr)
        # ds_r = np.array(ds_idx_arr[:SIZEOFDATASET])
        #
        # ds = df.iloc[ds_r, :]
        #
        # xyzt_idx = ds[['Points:0', 'Points:1', 'Points:2', 'Temperature']]
        # xyz_idx = ds[['Points:0', 'Points:1', 'Points:2']]
        # xy_idx = ds[['Points:0', 'Points:1']]
        # xyt_idx = ds[['Points:0', 'Points:1', 'Temperature']]
        # x_idx = ds[['Points:0']]
        # y_idx = ds[['Points:1']]
        # t_idx = ds[['Temperature']]
        # x_min = np.min(x_idx)
        # x_max = np.max(x_idx)
        # y_min = np.min(y_idx)
        # y_max = np.max(y_idx)
        #
        # y_max = 68.
        # BEGIN = [x_min, y_min]
        # END = [x_max, y_max]
        #
        # BEGIN = np.array(BEGIN).astype(dtype=np.float64).flatten()
        # END = np.array(END).astype(dtype=np.float64).flatten()
        #
        #
        # EPSILON = 0.2
        #
        # xyt_sel_idx = gpf.capture_close_3d_pts_with_2d_distance(xyt_idx, BEGIN, END, EPSILON)  # 2D distance, 3D pts
        #
        # if PLOTS:
        #     plts.plot_range_of_values_and_sel(12,4, xyt_idx, xyt_sel_idx)
        #     plts.plot_2d_observations(12, 12, xyt_idx, xyt_sel_idx, BEGIN, END)
        #     plts.plot_capture_line(12, 12, xyt_sel_idx, BEGIN, END)
        #     plts.plot_val_histogram(10, 4, np.array(t_idx).T.flatten())

        # -------------------------------------------


class Indirection:
    def __init__(self, config_path):
        super().__init__()
        self.config_path = config_path
        self.data_config = FromYAML(config_path).load_config()
        self.COVER_spatial = self.get('COVER_spatial')
        self.K_to_optimize = self.get('K_to_optimize')
        self.Kth = self.get('Kth')
        self.GEN_LOCAL_kernel = self.get('GEN_LOCAL_kernel')
        self.BETA_val = self.get('BETA_val')
        self.K_SENSORS = self.get('K_SENSORS')
        self.EPSILON_fraction = self.get('EPSILON_fraction')
        self.OPTIMIZATION_attempts = self.get('OPTIMIZATION_attempts')
        self.GRID_RANGE = self.get('GRID_RANGE')
        self.GRID_RANGE_x = self.get('GRID_RANGE_x')
        self.GRID_RANGE_y = self.get('GRID_RANGE_y')
        self.GRID_RANGE_z = self.get('GRID_RANGE_z')
        self.COORD_min_values = self.get('COORD_min_values')
        self.COORD_max_values = self.get('COORD_max_values')

    def get(self, key: str):
        return self.data_config.get(key, None)


class VGPCaseStudyOfficeData(TrainingData):
    def __init__(self, config_path: str):
        super(VGPCaseStudyOfficeData, self).__init__(config_path)
        self.norm = NormalizingParams()
        self.indirection = Indirection(config_path)

    def get_VGP_parameters(self):
        return (self.inducing_index_points, self.obs_index_points, self.obs,
                self.predictive_index_points, self.num_feature_dims)

    def get_VGP_parameters_from(self):
        return (self.num_training_points_, self.num_predictive_index_points_one_dir,
                self.num_predictive_index_points_, self.num_inducing_points_,
                self.feature_dims)

    def get_spatial_splits(self):
        #     COVER_spatial
        # global GEN_LOCAL_kernel
        # global BETA_val
        # GEN_LOCAL_kernel = True
        # BETA_val = 4.
        # COVER_spatial = [7, 7, 1]  # also size of covariance matrix in the end
        # TODO MOVE TO CONFIG
        tag = str(self.indirection.COVER_spatial[0]) + 'x' + str(self.indirection.COVER_spatial[1]) + 'x' + str(
            self.indirection.COVER_spatial[2]) + ('L' if self.indirection.GEN_LOCAL_kernel else 'F')
        if (not os.path.exists('./train-data')):
            os.mkdir('train-data')
        file_cov_vv = 'train-data/cov_vv_' + tag + '.csv'
        file_cov_idxs = 'train-data/placement_algorithm_xyz_cov_idxs' + tag + '.csv'
        file_delt_ch_it = 'train-data/placement_algorithm_cache' + tag + '.csv'
        SAVE_SELECTION_file = 'train-data/placement_algorithm_selection_idxs' + tag + '.csv'

        return self.indirection.COVER_spatial, file_cov_vv, file_cov_idxs, file_delt_ch_it, SAVE_SELECTION_file, \
               self.indirection.GEN_LOCAL_kernel, self.indirection.BETA_val

    def generate_predictive_index_points(self, num_points, coord_range=None):
        # USED TO BE: generate_5d_idx(num_predictive_index_points_, coord_range=coord_range)
        # num_points can be: num_predictive_index_points_, num_predictive_index_points_, num_training_points_
        idx_pts = np.random.uniform(0., 1., (num_points, 5)).astype(dtype=dtype)  # XY
        assert (idx_pts.shape == (num_points, 5))

        for i in range(5):
            [low, high] = self.coordinate_range[i]
            scale = high - low
            idx_pts[:, i] *= scale
            idx_pts[:, i] += low
        self.predictive_index_points = idx_pts[np.newaxis, ...]
        return self.predictive_index_points  # batch, num, coord

    def get_idx_obs_coord_range(self):
        df_ = pd.read_csv(os.path.join(os.path.dirname(os.path.dirname(__file__)), self.path_to_data), encoding='utf-8', engine='c')
        df_.drop(columns=['Unnamed: 0'], inplace=True)

        AVE_x, STD_x = df_['Points:0'].mean(), df_['Points:0'].std()
        AVE_y, STD_y = df_['Points:1'].mean(), df_['Points:1'].std()
        AVE_z, STD_z = df_['Points:2'].mean(), df_['Points:2'].std()
        AVE_t, STD_t = df_['Temperature_ave'].mean(), df_['Temperature_ave'].std()
        AVE_p, STD_p = df_['Pressure_ave'].mean(), df_['Pressure_ave'].std()
        print([[AVE_x, STD_x], [AVE_y, STD_y], [AVE_z, STD_z], [AVE_t, STD_t], [AVE_p, STD_p]])

        MIN_p0 = df_['Pressure_ave'].min()
        MAX_p0 = df_['Pressure_ave'].max()
        print('MIN_p0, MAX_p0', [MIN_p0, MAX_p0])

        # Normalization parameters: supposed to be used everywhere
        COLUMNS_MEAN, COLUMNS_STDEV, tracer_MEAN, tracer_STDEV = self.norm.get_normalizing_parameters()

        df_.loc[:, 'Points:0'] -= COLUMNS_MEAN[0]
        df_.loc[:, 'Points:0'] /= COLUMNS_STDEV[0]
        df_.loc[:, 'Points:1'] -= COLUMNS_MEAN[1]
        df_.loc[:, 'Points:1'] /= COLUMNS_STDEV[1]
        df_.loc[:, 'Points:2'] -= COLUMNS_MEAN[2]
        df_.loc[:, 'Points:2'] /= COLUMNS_STDEV[2]
        df_.loc[:, 'Temperature_ave'] -= COLUMNS_MEAN[3]
        df_.loc[:, 'Temperature_ave'] /= COLUMNS_STDEV[3]
        df_.loc[:, 'Pressure_ave'] -= COLUMNS_MEAN[4]
        AVE_p1 = df_['Pressure_ave'].mean()
        print('AVE_p1=', AVE_p1)
        df_.loc[:, 'Pressure_ave'] /= COLUMNS_STDEV[4]
        AVE_p2, STD_p2 = df_['Pressure_ave'].mean(), df_['Pressure_ave'].std()
        print('AVE_p2, STD_p2=', [AVE_p2, STD_p2])

        df_.loc[:, 'Tracer_ave'] -= tracer_MEAN[0]
        df_.loc[:, 'Tracer_ave'] /= tracer_STDEV[0]

        MIN_x = df_['Points:0'].min()
        MIN_y = df_['Points:1'].min()
        MIN_z = df_['Points:2'].min()  # TODO should this be /5 # 0.0       # because I2 = 1
        MIN_t = df_['Temperature_ave'].min()
        MIN_p = df_['Pressure_ave'].min()

        MAX_x = df_['Points:0'].max()
        MAX_y = df_['Points:1'].max()
        MAX_z = df_['Points:2'].max()  # TODO should this be *4/5  # 0.1
        MAX_t = df_['Temperature_ave'].max()
        MAX_p = df_['Pressure_ave'].max()

        # coord_range = [[-80., 0.], [-80., 0.]]  # [[-2., 2.], [-4., 4.]]  # [[-10., 10.], [-7., 7.]]
        coord_range = [[MIN_x, MAX_x], [MIN_y, MAX_y], [MIN_z, MAX_z], [MIN_t, MAX_t], [MIN_p, MAX_p]]
        print('coord_range=', coord_range)

        MIN_tr = df_['Tracer_ave'].min()
        MAX_tr = df_['Tracer_ave'].max()
        print('tracer_range=', [MIN_tr, MAX_tr])

        obs_idx_pts = df_.loc[:, ['Points:0', 'Points:1', 'Points:2', 'Temperature_ave', 'Pressure_ave']].to_numpy()[np.newaxis, ...]
        obs = df_.loc[:, 'Tracer_ave'].to_numpy()[np.newaxis, ...]
        self.obs = obs
        self.obs_index_points = obs_idx_pts
        self.coordinate_range = coord_range
        return obs_idx_pts, obs, coord_range


class VGPCaseStudySinusoidData(TrainingData):
    def __init__(self, config_path: str):
        super(VGPCaseStudySinusoidData, self).__init__(config_path)

    def generate_training_data_sin_2d(self):  # TODO merge with generate_training_data
        self.all_index_points, self.all_observations = ut.SinusoidData.generate_1d_sin_data(
            self.num_training_points_ + self.num_test_points_,
            self.generated_observation_noise_variance,
            self.coordinate_range
        )
        self.obs = self.all_observations[:self.num_training_points_][np.newaxis, ...]
        self.obs_index_points = self.all_index_points[:self.num_training_points_][np.newaxis, ...]
        self.inducing_index_points = ut.SinusoidData.generate_1d_inducing_index_points(self.num_predictive_index_points_, self.coordinate_range)
        self.predictive_index_points = ut.SinusoidData.generate_1d_inducing_index_points(self.num_predictive_index_points_, self.coordinate_range)
        self.test_index_points = self.all_index_points[self.num_test_points_:]  # TODO num_test_points_
        self.test_obs = self.all_observations[self.num_test_points_:]  # num_test_points_

    def generate_training_data(self, **kwargs):
            self.all_index_points, self.all_observations = ut.SinusoidData.generate_3d_sin_index_observation_points(
                self.num_test_points_ + self.num_training_points_,
                self.generated_observation_noise_variance,
                self.coordinate_range
            )
            self.obs = self.all_observations[:self.num_training_points_][np.newaxis, ...]
            self.obs_index_points = self.all_index_points[:self.num_training_points_][np.newaxis, ...]
            self.inducing_index_points = ut.SinusoidData.generate_2d_index_points(self.num_predictive_index_points_, self.coordinate_range)
            self.predictive_index_points = ut.SinusoidData.generate_2d_index_points(self.num_predictive_index_points_, self.coordinate_range)
            self.test_index_points = self.all_index_points[self.num_test_points_:]  # TODO num_test_points_
            self.test_obs = self.all_observations[self.num_test_points_:]  # num_test_points_

    def get_VGP_parameters(self):
        return (self.inducing_index_points, self.obs_index_points, self.obs,
                self.predictive_index_points, self.num_feature_dims)


def generate_5d_idx(num_train_pts,
                    coord_range,
                    dtype=np.float64):

    idx_pts = np.random.uniform(0., 1., (num_train_pts, 5)).astype(dtype=dtype)  # XY
    assert (idx_pts.shape == (num_train_pts, 5))

    for i in range(5):
        [low, high] = coord_range[i]
        scale = high - low
        idx_pts[:, i] *= scale
        idx_pts[:, i] += low

    return idx_pts[np.newaxis, ...]  # batch, num, coord


def graph_get_vgp_input_xyztp(loc_xyz, vec_pt):
    # xyz_ = tf.constant([-3., -2.99, -2.98])
    dim0 = vec_pt.shape[0]
    val_0 = loc_xyz[0]
    val_1 = loc_xyz[1]
    val_2 = loc_xyz[2]
    fx = tf.fill(dims=[dim0, 1], value=val_0)
    fy = tf.fill(dims=[dim0, 1], value=val_1)
    fz = tf.fill(dims=[dim0, 1], value=val_2)

    fx_ = tf.cast(tf.reshape(fx, shape=[-1], name="fx"), dtype=tf.float64)
    fy_ = tf.cast(tf.reshape(fy, shape=[-1], name="fy"), dtype=tf.float64)
    fz_ = tf.cast(tf.reshape(fz, shape=[-1], name="fz"), dtype=tf.float64)

    t_ = tf.slice(vec_pt, [0, 1], [dim0, 1])
    p_ = tf.slice(vec_pt, [0, 0], [dim0, 1])
    ft_ = tf.cast(tf.reshape(t_, shape=[-1], name="ft_"), dtype=tf.float64)
    fp_ = tf.cast(tf.reshape(p_, shape=[-1], name="fp_"), dtype=tf.float64)

    fxyztp = tf.stack([fx_, fy_, fz_, ft_, fp_], axis=1, name="fxyztp")

    return fxyztp


def from_normalized_coord_to_denormalized_coord(kth_norm):
    eXcol, eYcol, eZcol = 0, 1, 2
    COLUMNS_MEAN, COLUMNS_STDEV, tracer_MEAN, tracer_STDEV = get_normalizing_parameters()
    x_denorm = kth_norm[eXcol] * COLUMNS_STDEV[eXcol] + COLUMNS_MEAN[eXcol]
    y_denorm = kth_norm[eYcol] * COLUMNS_STDEV[eYcol] + COLUMNS_MEAN[eYcol]
    z_denorm = kth_norm[eZcol] * COLUMNS_STDEV[eZcol] + COLUMNS_MEAN[eZcol]
    return x_denorm, y_denorm, z_denorm  # is actual xyz coordinate value, not index


def get_normalizing_parameters():
    # Normalization parameters: supposed to be used everywhere
    COLUMNS_MEAN = np.array([225.11911961, 66.54242329, 7.32220308, 17.6831950, 3.2987280])
    COLUMNS_STDEV = np.array([2.17425109, 1.71791751, 0.96232142, 2.2824182, 1.5928748])
    tracer_MEAN = np.array([0.0018159087825037148])
    tracer_STDEV = np.array([0.0007434639347162126])
    return COLUMNS_MEAN, COLUMNS_STDEV, tracer_MEAN, tracer_STDEV


def get_spatial_splits(indirection):
    #     COVER_spatial
    # global GEN_LOCAL_kernel
    # global BETA_val

    # GEN_LOCAL_kernel = True
    # BETA_val = 4.
    # COVER_spatial = [7, 7, 1]  # also size of covariance matrix in the end
    tag = str(indirection['COVER_spatial'][0]) + 'x' + str(indirection['COVER_spatial'][1]) + 'x' + str(
        indirection['COVER_spatial'][2]) + ('L' if indirection['GEN_LOCAL_kernel'] else 'F')
    if (not os.path.exists('./train-data')):
        os.mkdir('train-data')
    file_cov_vv = 'train-data/cov_vv_' + tag + '.csv'
    file_cov_idxs = 'train-data/placement_algorithm_xyz_cov_idxs' + tag + '.csv'
    file_delt_ch_it = 'train-data/placement_algorithm_cache' + tag + '.csv'
    SAVE_SELECTION_file = 'train-data/placement_algorithm_selection_idxs' + tag + '.csv'
    return indirection['COVER_spatial'], file_cov_vv, file_cov_idxs, file_delt_ch_it, SAVE_SELECTION_file, \
           indirection['GEN_LOCAL_kernel'], indirection['BETA_val']


def from_sel_idx_in_grid_to_denormalized_coordinate(kth_selection_idx, grid):
    # 1.  get normalized coord -> then get denormalized coord
    # 2.  get denormalized coord
    # denormalize -> * STDEV, + MEAN
    # for example: kth_norm_sel = [-2,0,2]
    # kth_norm_sel = from_sel_idx_to_normalized_coordinate(kth_selection, cover_spatial)

    # assert kth_selection_idx.shape == 1
    kth_norm_coord = from_sel_idx_in_grid_to_normalized_coordinate(kth_selection_idx, grid)
    x, y, z = from_normalized_coord_to_denormalized_coord(kth_norm_coord)
    return x, y, z  # is actual xyz coordinate value, not index


def from_sel_idx_in_grid_to_normalized_coordinate(sel, grid):
    i0, i1, i2 = from_sel_to_grid_idxptr(sel, grid)  # i0, i1, i2
    i0 = int(i0)
    i1 = int(i1)
    i2 = int(i2)
    Xn = grid[i0, i1, i2, 0]
    Yn = grid[i0, i1, i2, 1]
    Zn = grid[i0, i1, i2, 2]
    return Xn, Yn, Zn


def from_sel_to_grid_idxptr(sel, grid):
    I0 = grid.shape[0]  # COVER_spatial = number of gridpoints for x axis, ...
    I1 = grid.shape[1]  # y-axis
    I2 = grid.shape[2]  # z-axis
    # Iterate with Z first, then Y, then X !
    strde0 = I2 * I1
    strde1 = I2
    strde2 = 1
    i0 = sel // strde0
    i1 = (sel - i0 * strde0) // strde1
    i2 = (sel - i0 * strde0 - i1 * strde1) // strde2
    return int(i0), int(i1), int(i2)


def from_normalized_coordinate_to_ID(df, kx, ky, kz):
    # minimize = np.sqrt( np.square(df_x - kx)  )
    comp = np.sqrt((df['Points:0'] - kx)**2 + (df['Points:1'] - ky)**2 + (df['Points:2'] - kz)**2)
    nearxyz = df.iloc[(comp).abs().argsort()[:10]]
    nearest_ID = pd.DataFrame(nearxyz, columns=['vtkOriginalPointIds'])[:1]
    return nearest_ID


# ===========================================================================
# from selection to vtk ID:
# ===========================================================================
# sel_idx was chosen in a given grid
# grid contains normalized coordinates
# normalized coordinates are denormalized without need for grid
# denormalized coordinates have a nearest simulated coordinate with an ID.
# ===========================================================================
def from_selection_in_grid_to_ID(selection_array, grid):
    # 1.  get 1 selection -> denormalized coordinate
    # 2.  denormalized coordinate to nearest ID
    # repeat for all selections.

    # To identify xyz coordinates
    dset = pd.read_csv(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), 'artifacts/data', '1500_timestep.17.csv'),
        encoding='utf-8', engine='c')

    collection_IDs = np.array([])
    for selection_idx in selection_array:
        # assert len(selection_idx) == 1
        x, y, z = from_sel_idx_in_grid_to_denormalized_coordinate(selection_idx, grid)
        original_nearest_ID = from_normalized_coordinate_to_ID(dset, x, y, z)
        collection_IDs = np.append(collection_IDs, original_nearest_ID)
    return collection_IDs


def init_grid(indirection):
    GRID_range_x = indirection.GRID_RANGE_x
    GRID_range_y = indirection.GRID_RANGE_y
    GRID_range_z = indirection.GRID_RANGE_z
    COVER_spatial = indirection.COVER_spatial
    optimal_grid_values = np.zeros((COVER_spatial[0], COVER_spatial[1], COVER_spatial[2], 3))
    nplinsp_x = np.linspace(GRID_range_x[0], GRID_range_x[1], COVER_spatial[0])
    nplinsp_y = np.linspace(GRID_range_y[0], GRID_range_y[1], COVER_spatial[1])
    nplinsp_z = np.linspace(GRID_range_z[0], GRID_range_z[1], COVER_spatial[2])
    for z0 in range(COVER_spatial[0]):
        for z1 in range(COVER_spatial[1]):
            for z2 in range(COVER_spatial[2]):
                # print("optimal_grid_values[z0, z1, z2, :] before", optimal_grid_values[z0, z1, z2, :])
                optimal_grid_values[z0, z1, z2, 0] = nplinsp_x[z0]  #, nplinsp_y[z1], nplinsp_z[z2]
                optimal_grid_values[z0, z1, z2, 1] = nplinsp_y[z1]
                optimal_grid_values[z0, z1, z2, 2] = nplinsp_z[z2]
                # print("optimal_grid_values[z0, z1, z2, :] after", optimal_grid_values[z0, z1, z2, :])
    return optimal_grid_values